drop schema if exists instant_messaging;
create schema instant_messaging;
use instant_messaging;
drop table if exists user;
drop table if exists role;
drop table if exists user_roles;
drop table if exists private_message;
drop table if exists private_channel;
drop table if exists group_message;
drop table if exists group_channel;
drop table if exists group_channel_user;
drop table if exists broadcast_message;
drop table if exists broadcast_channel;


CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_users_username` (`username`),
  UNIQUE KEY `uk_users_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table role (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  NAME VARCHAR(255) NOT NULL
);

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_roles_role_id` (`role_id`),
  CONSTRAINT `fk_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table private_message (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  USER_FROM_ID BIGINT NOT NULL,
  USER_TO_ID BIGINT NOT NULL,
  CHANNEL_ID BIGINT NOT NULL,
  CONTENT TEXT NOT NULL
);

create table private_channel (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  USER_FROM_ID BIGINT NOT NULL,
  USER_TO_ID BIGINT NOT NULL,
  UNIQUE (USER_FROM_ID, USER_TO_ID)
);


create table group_message (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  USER_FROM_ID BIGINT NOT NULL,
  GROUP_CHANNEL_ID BIGINT NOT NULL,
  CONTENT TEXT NOT NULL
);

create table group_channel (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  TITLE VARCHAR(255) NOT NULL,
  OWNER_ID BIGINT NOT NULL
);

create table group_channel_user (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  GROUP_ID BIGINT NOT NULL,
  USER_ID BIGINT NOT NULL,
  FOREIGN KEY (GROUP_ID) REFERENCES group_channel (ID),
  FOREIGN KEY (USER_ID) REFERENCES user (ID)
);


create table broadcast_message (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  USER_FROM_ID BIGINT NOT NULL,
  CHANNEL_ID BIGINT NOT NULL,
  CONTENT TEXT NOT NULL
);

create table broadcast_channel (
  ID BIGINT PRIMARY KEY AUTO_INCREMENT,
  TITLE VARCHAR(255) NOT NULL,
  OWNER_ID BIGINT NOT NULL
);





INSERT IGNORE INTO role(name) VALUES('ROLE_USER');
INSERT IGNORE INTO role(name) VALUES('ROLE_ADMIN');