package org.aghayev.controller;

import io.crnk.core.engine.registry.RegistryEntry;
import io.crnk.core.engine.registry.ResourceRegistry;
import io.crnk.spring.boot.v3.CrnkConfigV3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nijat Aghayev on 27.08.19.
 */
@RestController
@Import({CrnkConfigV3.class})
public class CrnkController {

    @Autowired
    private ResourceRegistry resourceRegistry;

    @RequestMapping("/api/resources-info")
    public Map<String, String> getResources() {
        Map<String, String> result = new HashMap<>();
        // Add all resources
        for (RegistryEntry entry : resourceRegistry.getResources()) {
            result.put(entry.getResourceInformation().getResourceType(),
                    resourceRegistry.getResourceUrl(entry.getResourceInformation()));
        }

        return result;
    }
}