package org.aghayev.controller;

import org.aghayev.exception.UserIsSameException;
import org.aghayev.payload.GroupChannelInitializationDTO;
import org.aghayev.payload.GroupMessageDTO;
import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.util.GroupChatService;
import org.aghayev.util.JSONResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@RestController
public class GroupChatController {
    private final GroupChatService chatService;
    private JSONResponseHelper jsonResponseHelper;

    @Autowired
    public GroupChatController(GroupChatService chatService,
                               JSONResponseHelper jsonResponseHelper) {
        this.chatService = chatService;
        this.jsonResponseHelper = jsonResponseHelper;
    }

    @PutMapping(value="/api/group-chat/channel",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> establishGroupChannel(@RequestBody GroupChannelInitializationDTO payload)
    throws UserIsSameException {
        Long channelId = chatService.establishChatSesion(payload);
        return jsonResponseHelper.createResponse(channelId, HttpStatus.OK);
    }

    @MessageMapping("/group.chat.{channelId}")
    @SendTo("/topic/group.chat.{channelId}")
    public GroupMessageDTO sendMessage(@DestinationVariable Long channelId,
                                    @Payload GroupMessageDTO chatMessage) {
        chatService.submitMessage(chatMessage);
        return chatMessage;
    }

    @GetMapping(value="/api/group-chat/channel/{channelId}",
            produces = "application/json")
    public ResponseEntity<String> getExistingChatMessages(@PathVariable("channelId") long channelId) {
        List<GroupMessageDTO>  ret = chatService.getExistingChatMessages(channelId);
        return jsonResponseHelper.createResponse(ret, HttpStatus.OK);
    }

    @RequestMapping(value="/api/group-chat/message-count/{channelIds}/{userId}",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<String> getMessageCountsForChannelsByUserId(@PathVariable("channelIds") String channelIds,
                                                                       @PathVariable("userId") long userId) {
        List<MessageCounterDTO> ret = chatService.getMessageCountsForChannelsByUserId(channelIds, userId);
        return jsonResponseHelper.createResponse(ret, HttpStatus.OK);
    }

}
