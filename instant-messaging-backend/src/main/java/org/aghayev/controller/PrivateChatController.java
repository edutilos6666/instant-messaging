package org.aghayev.controller;

import org.aghayev.exception.UserIsSameException;
import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.payload.PrivateChatInitializationDTO;
import org.aghayev.payload.PrivateMessageDTO;
import org.aghayev.util.JSONResponseHelper;
import org.aghayev.util.PrivateChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Controller
public class PrivateChatController {
    private final PrivateChatService chatService;
    private final JSONResponseHelper jsonResponseHelper;

    @Autowired
    public PrivateChatController(PrivateChatService chatService, JSONResponseHelper jsonResponseHelper) {
        this.chatService = chatService;
        this.jsonResponseHelper = jsonResponseHelper;
    }

    @PutMapping(value="/api/private-chat/channel",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> establishPrivateChannel(@RequestBody PrivateChatInitializationDTO payload)
    throws UserIsSameException {
        Long channelId = chatService.establishChatSesion(payload);
        return jsonResponseHelper.createResponse(channelId, HttpStatus.OK);
    }

    @MessageMapping("/private.chat.{channelId}")
    @SendTo("/topic/private.chat.{channelId}")
    public PrivateMessageDTO sendMessage(@DestinationVariable Long channelId,
                                   @Payload PrivateMessageDTO chatMessage) {
        chatService.submitMessage(channelId, chatMessage);
        return chatMessage;
    }

    @GetMapping(value="/api/private-chat/channel/{channelId}",
            produces = "application/json")
    public ResponseEntity<String> getExistingChatMessages(@PathVariable("channelId") long channelId) {
        List<PrivateMessageDTO> res = chatService.getExistingChatMessages(channelId);
        return jsonResponseHelper.createResponse(res, HttpStatus.OK);
    }


    @GetMapping(value="/api/private-chat/message-count/{channelIds}/{userId}",
            produces = "application/json")
    public ResponseEntity<String> getMessageCountsForChannelsByUserId(@PathVariable("channelIds") String channelIds,
                                                                       @PathVariable("userId") long userId) {
        List<MessageCounterDTO> ret =
                chatService.getMessageCountsForChannelsByUserId(channelIds, userId);
        return jsonResponseHelper.createResponse(ret, HttpStatus.OK);
    }

}
