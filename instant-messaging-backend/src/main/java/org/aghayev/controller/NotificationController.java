package org.aghayev.controller;

import org.aghayev.payload.MessageCounterDTO;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@RestController
public class NotificationController {
    @SendTo("/topic/user.notifcation.private.channel.{userId}")
    public String notifyUserAboutPrivateChannel(@DestinationVariable("userId") long userId,
                                                @Payload String message) {
        return message;
    }

    @SendTo("/topic/user.notifcation.private.message.{userId}")
    public MessageCounterDTO notifyUsersAfterPrivateMessageSent(@DestinationVariable("userId") long userId,
                                                   @Payload MessageCounterDTO message) {
        return message;
    }


    @SendTo("/topic/user.notifcation.group.channel.{userId}")
    public String notifyUserAboutGroupChannel(@DestinationVariable("userId") long userId,
                                                @Payload String message) {
        return message;
    }

    @SendTo("/topic/user.notifcation.group.message.{userId}")
    public MessageCounterDTO notifyUsersAfterGroupMessageSent(@DestinationVariable("userId") long userId,
                                                   @Payload MessageCounterDTO message) {
        return message;
    }

    @SendTo("/topic/user.notification.broadcast.channel")
    public String notifyUsersAboutBroadcastChannel(@Payload String message) {
        return message;
    }

    @SendTo("/topic/user.notifcation.broadcast.message")
    public MessageCounterDTO notifyUsersAfterBroadcastSent(@Payload MessageCounterDTO message) {
        return message;
    }
}
