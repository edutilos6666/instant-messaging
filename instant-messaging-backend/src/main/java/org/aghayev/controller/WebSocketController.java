package org.aghayev.controller;

import org.aghayev.model.ChatMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class WebSocketController {

    private final SimpMessagingTemplate template;

    @Autowired
    WebSocketController(SimpMessagingTemplate template){
        this.template = template;
    }

    @MessageMapping("/chat2.sendMessage")
    @SendTo("/topic/greetings")
    public ChatMessage onReceivedMesage(@Payload ChatMessage chatMessage){
//        this.template.convertAndSend("/topic/greetings",  new SimpleDateFormat("HH:mm:ss").format(new Date())+"- "+message);
        return chatMessage;
    }

    @MessageMapping("/chat2.addUser")
    @SendTo("/topic/greetings")
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor messageHeaderAccessor) {
        messageHeaderAccessor.getSessionAttributes().putIfAbsent("username", chatMessage.getSender());
        messageHeaderAccessor.getSessionAttributes().putIfAbsent("type", chatMessage.getType());
        return chatMessage;
    }
}
