package org.aghayev.controller;

import org.aghayev.exception.UserIsSameException;
import org.aghayev.payload.BroadcastChannelDTO;
import org.aghayev.payload.BroadcastMessageDTO;
import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.util.BroadcastChatService;
import org.aghayev.util.JSONResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@RestController
public class BroadcastChatController {
    private final BroadcastChatService chatService;
    private JSONResponseHelper jsonResponseHelper;

    @Autowired
    public BroadcastChatController(BroadcastChatService chatService,
                                   JSONResponseHelper jsonResponseHelper) {
        this.chatService = chatService;
        this.jsonResponseHelper = jsonResponseHelper;
    }

    @PutMapping(value="/api/broadcast-chat/channel",
            consumes = "application/json",
            produces = "application/json")
    public ResponseEntity<String> establishBroadcastChannel(@RequestBody BroadcastChannelDTO payload)
    throws UserIsSameException {
        Long channelId = chatService.establishChatSesion(payload);
        return jsonResponseHelper.createResponse(channelId, HttpStatus.OK);
    }

    @MessageMapping("/broadcast.chat.{channelId}")
    @SendTo("/topic/broadcast.chat.{channelId}")
    public BroadcastMessageDTO sendMessage(@DestinationVariable Long channelId,
                                    @Payload BroadcastMessageDTO chatMessage) {
        chatService.submitMessage(chatMessage);
        return chatMessage;
    }

    @GetMapping(value="/api/broadcast-chat/channel/{channelId}",
            produces = "application/json")
    public ResponseEntity<String> getExistingChatMessages(@PathVariable("channelId") long channelId) {
        List<BroadcastMessageDTO>  ret = chatService.getExistingChatMessages(channelId);
        return jsonResponseHelper.createResponse(ret, HttpStatus.OK);
    }

    @RequestMapping(value="/api/broadcast-chat/message-count",
            method = RequestMethod.GET,
            produces = "application/json")
    public ResponseEntity<String> getMessageCountsForChannelsByUserId() {
        List<MessageCounterDTO> ret = chatService.getMessageCountsForChannels();
        return jsonResponseHelper.createResponse(ret, HttpStatus.OK);
    }

}
