package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.PrivateMessage;
import org.aghayev.repository.PrivateMessageRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class PrivateMessageEndpoint extends ResourceRepositoryBase<PrivateMessage, Long> {
    @Autowired
    private PrivateMessageRepository privateMessageRepository;
    @Autowired
    private EndpointResponseConstructor endpointResponseConstructor;

    protected PrivateMessageEndpoint() {
        super(PrivateMessage.class);
    }

    @Override
    public ResourceList<PrivateMessage> findAll(QuerySpec querySpec) {
        Long offset = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = privateMessageRepository.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> userFromId = QuerySpecUtil.getFilterValue(querySpec, "userFromId");
        Optional<String> userToId = QuerySpecUtil.getFilterValue(querySpec, "userToId");
        Page<PrivateMessage> ret = null;
        if(userFromId.isPresent() && userToId.isPresent()) {
            ret = privateMessageRepository
                    .getExistingChatMessagesBetweenPageable(Long.parseLong(userFromId.get()),
                            Long.parseLong(userToId.get()),
                            new OffsetBasedPageRequest(offset, limit));
        } else {
            ret = privateMessageRepository
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }

        return endpointResponseConstructor.constructResponseForFindAll(ret);
    }
}
