package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.PrivateChannel;
import org.aghayev.repository.PrivateChannelRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class PrivateChannelEndpoint extends ResourceRepositoryBase<PrivateChannel, Long> {
    @Autowired
    private PrivateChannelRepository privateChannelRepository;
    @Autowired
    private EndpointResponseConstructor endpointResponseConstructor;

    protected PrivateChannelEndpoint() {
        super(PrivateChannel.class);
    }

    @Override
    public ResourceList<PrivateChannel> findAll(QuerySpec querySpec) {
        Long offset  = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = privateChannelRepository.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> userId = QuerySpecUtil.getFilterValue(querySpec, "userId");
        Page<PrivateChannel> ret = null;
        if(userId.isPresent()) {
            ret = privateChannelRepository
                    .findPrivateChannelByUserIdWithPage(Long.parseLong(userId.get()),
                            new OffsetBasedPageRequest(offset, limit));
        } else {
            ret = privateChannelRepository
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }
        return this.endpointResponseConstructor.constructResponseForFindAll(ret);
    }

    @Override
    public PrivateChannel findOne(Long id, QuerySpec querySpec) {
        return privateChannelRepository.findById(id).get();
    }
}
