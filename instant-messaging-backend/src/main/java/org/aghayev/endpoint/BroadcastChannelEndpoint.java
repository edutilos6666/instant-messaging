package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.BroadcastChannel;
import org.aghayev.repository.BroadcastChannelRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class BroadcastChannelEndpoint extends ResourceRepositoryBase<BroadcastChannel, Long> {
    private final BroadcastChannelRepository bcRepo;
    private final EndpointResponseConstructor endpointResponseConstructor;

    @Autowired
    protected BroadcastChannelEndpoint(
            BroadcastChannelRepository bcRepo,
            EndpointResponseConstructor endpointResponseConstructor) {
        super(BroadcastChannel.class);
        this.bcRepo = bcRepo;
        this.endpointResponseConstructor = endpointResponseConstructor;
    }

    @Override
    public ResourceList<BroadcastChannel> findAll(QuerySpec querySpec) {
        Long offset  = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = bcRepo.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> title = QuerySpecUtil.getFilterValue(querySpec, "title");
        Page<BroadcastChannel> ret = null;
        if(title.isPresent()) {
            ret = bcRepo.findAllByTitle(title.get(),
                    new OffsetBasedPageRequest(offset, limit));
        } else {
            ret = bcRepo
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }
        return this.endpointResponseConstructor.constructResponseForFindAll(ret);
    }


    @Override
    public BroadcastChannel findOne(Long id, QuerySpec querySpec) {
        return bcRepo.findById(id).get();
    }
}
