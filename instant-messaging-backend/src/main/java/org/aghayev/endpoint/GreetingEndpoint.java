package org.aghayev.endpoint;

import java.util.HashMap;
import java.util.Map;

import org.aghayev.model.Greeting;
import org.springframework.stereotype.Component;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;


/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class GreetingEndpoint extends ResourceRepositoryBase<Greeting, Long> {

    private Map<Long, Greeting> greetings = new HashMap<>();

    public GreetingEndpoint() {
        super(Greeting.class);

        greetings.put(1L, new Greeting(1L, "Hello World!"));
    }

    @Override
    public synchronized ResourceList<Greeting> findAll(QuerySpec querySpec) {
        return querySpec.apply(greetings.values());
    }
}