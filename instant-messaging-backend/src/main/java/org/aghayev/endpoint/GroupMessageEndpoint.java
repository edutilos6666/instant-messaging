package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.GroupMessage;
import org.aghayev.repository.GroupMessageRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class GroupMessageEndpoint extends ResourceRepositoryBase<GroupMessage, Long> {
    private final GroupMessageRepository groupMessageRepository;
    private final EndpointResponseConstructor endpointResponseConstructor;

    @Autowired
    protected GroupMessageEndpoint(GroupMessageRepository groupMessageRepository, EndpointResponseConstructor endpointResponseConstructor) {
        super(GroupMessage.class);
        this.groupMessageRepository = groupMessageRepository;
        this.endpointResponseConstructor = endpointResponseConstructor;
    }

    @Override
    public ResourceList<GroupMessage> findAll(QuerySpec querySpec) {
        Long offset  = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = groupMessageRepository.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> groupChannelId = QuerySpecUtil.getFilterValue(querySpec, "groupChannelId");
        Page<GroupMessage> ret = null;
        if(groupChannelId.isPresent()) {
            ret = groupMessageRepository
                    .getExistingChatMessagesInGroupChannelWithPage(
                            Long.parseLong(groupChannelId.get()),
                            new OffsetBasedPageRequest(offset, limit));
        } else {
            ret = groupMessageRepository
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }
        return this.endpointResponseConstructor.constructResponseForFindAll(ret);
    }

    @Override
    public GroupMessage findOne(Long id, QuerySpec querySpec) {
        return groupMessageRepository.findById(id).get();
    }
}
