package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.GroupChannel;
import org.aghayev.model.User;
import org.aghayev.repository.GroupChannelRepository;
import org.aghayev.repository.UserRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class GroupChannelEndpoint extends ResourceRepositoryBase<GroupChannel, Long> {
    private final GroupChannelRepository groupChannelRepository;
    private final EndpointResponseConstructor endpointResponseConstructor;
    private final UserRepository userRepository;

    @Autowired
    protected GroupChannelEndpoint(
            GroupChannelRepository groupChannelRepository,
            EndpointResponseConstructor endpointResponseConstructor,
            UserRepository userRepository) {
        super(GroupChannel.class);
        this.groupChannelRepository = groupChannelRepository;
        this.endpointResponseConstructor = endpointResponseConstructor;
        this.userRepository = userRepository;
    }

    @Override
    public ResourceList<GroupChannel> findAll(QuerySpec querySpec) {
        Long offset  = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = groupChannelRepository.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> userId = QuerySpecUtil.getFilterValue(querySpec, "userId");
        Optional<String> title = QuerySpecUtil.getFilterValue(querySpec, "title");
        Page<GroupChannel> ret = null;
        if(userId.isPresent()) {
            User user = userRepository.findById(Long.parseLong(userId.get())).get();
            ret = groupChannelRepository
                    .findAllByUserId(user,
                            new OffsetBasedPageRequest(offset, limit));
        } else if(title.isPresent()) {
            ret = groupChannelRepository.findAllByTitle(title.get(),
                    new OffsetBasedPageRequest(offset, limit));
        } else {
            ret = groupChannelRepository
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }
        return this.endpointResponseConstructor.constructResponseForFindAll(ret);
    }


    @Override
    public GroupChannel findOne(Long id, QuerySpec querySpec) {
        return groupChannelRepository.findById(id).get();
    }
}
