package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.User;
import org.aghayev.repository.UserRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Component
public class UserEndpoint extends ResourceRepositoryBase<User, Long> {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EndpointResponseConstructor endpointResponseConstructor;

    protected UserEndpoint() {
        super(User.class);
    }

    @Override
    public ResourceList<User> findAll(QuerySpec querySpec) {
        Long offset  = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = userRepository.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> username = QuerySpecUtil.getFilterValue(querySpec, "username");
        Optional<Boolean> onlyCount = QuerySpecUtil.getFilterValue(querySpec, "onlyCount");
        Page<User> ret = null;
        if(username.isPresent()) {
            ret = userRepository
                    .findByUsernameOrEmail(username.get(), null, new OffsetBasedPageRequest(offset, limit));
        } else if(onlyCount.isPresent()) {
            ret = new PageImpl<>(Collections.emptyList(), new OffsetBasedPageRequest(offset, limit), limit);
        } else {
            ret = userRepository
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }

        return this.endpointResponseConstructor.constructResponseForFindAll(ret);
    }


    @Override
    public User findOne(Long id, QuerySpec querySpec) {
        return this.userRepository.findById(id).get();
    }


}
