package org.aghayev.endpoint;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.aghayev.model.BroadcastMessage;
import org.aghayev.repository.BroadcastMessageRepository;
import org.aghayev.util.EndpointResponseConstructor;
import org.aghayev.util.OffsetBasedPageRequest;
import org.aghayev.util.QuerySpecUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Component
public class BroadcastMessageEndpoint extends ResourceRepositoryBase<BroadcastMessage, Long> {
    private final BroadcastMessageRepository bmRepo;
    private final EndpointResponseConstructor endpointResponseConstructor;

    @Autowired
    protected BroadcastMessageEndpoint(
            BroadcastMessageRepository bmRepo,
            EndpointResponseConstructor endpointResponseConstructor) {
        super(BroadcastMessage.class);
        this.bmRepo = bmRepo;
        this.endpointResponseConstructor = endpointResponseConstructor;
    }

    @Override
    public ResourceList<BroadcastMessage> findAll(QuerySpec querySpec) {
        Long offset  = querySpec.getOffset();
        Long limit = querySpec.getLimit();
        if(limit == null) limit = bmRepo.count();
        if(limit == 0) limit = offset + 1;
        Optional<String> channelId = QuerySpecUtil.getFilterValue(querySpec, "channelId");
        Page<BroadcastMessage> ret = null;
        if(channelId.isPresent()) {
            ret = bmRepo
                    .getExistingChatMessagesInChannelWithPage(
                            Long.parseLong(channelId.get()),
                            new OffsetBasedPageRequest(offset, limit));
        } else {
            ret = bmRepo
                    .findAll(new OffsetBasedPageRequest(offset, limit));
        }
        return this.endpointResponseConstructor.constructResponseForFindAll(ret);
    }

    @Override
    public BroadcastMessage findOne(Long id, QuerySpec querySpec) {
        return bmRepo.findById(id).get();
    }
}
