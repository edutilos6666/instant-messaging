package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class MessageCounterDTO {
    private long channelId;
    private long messageCount;

    public MessageCounterDTO(long channelId, long messageCount) {
        this.channelId = channelId;
        this.messageCount = messageCount;
    }
}
