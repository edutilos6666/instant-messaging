package org.aghayev.payload;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserIdentityAvailability {
    private final Boolean available;
}
