package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class PrivateChatInitializationDTO {
    private long userFromId;
    private long userToId;

    public PrivateChatInitializationDTO(long userFromId, long userToId) {
        this.userFromId = userFromId;
        this.userToId = userToId;
    }
}
