package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */

@Getter
@Setter
@NoArgsConstructor
public class GroupChannelInitializationDTO {
    private String title;
    private Long ownerId;
    private List<Long> membersIds;

    public GroupChannelInitializationDTO(String title, Long ownerId, List<Long> membersIds) {
        this.title = title;
        this.ownerId = ownerId;
        this.membersIds = membersIds;
    }
}
