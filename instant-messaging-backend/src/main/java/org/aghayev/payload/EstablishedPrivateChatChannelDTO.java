package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */

@Getter
@Setter
@NoArgsConstructor
public class EstablishedPrivateChatChannelDTO {
    private long channelId;
    private long userFromId;
    private long userToId;

    public EstablishedPrivateChatChannelDTO(long channelId, long userFromId, long userToId) {
        this.channelId = channelId;
        this.userFromId = userFromId;
        this.userToId = userToId;
    }
}
