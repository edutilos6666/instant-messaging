package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */

@Getter
@Setter
@NoArgsConstructor
public class BroadcastChannelDTO {
    private String title;
    private long ownerId;

    public BroadcastChannelDTO(String title, long ownerId) {
        this.title = title;
        this.ownerId = ownerId;
    }
}
