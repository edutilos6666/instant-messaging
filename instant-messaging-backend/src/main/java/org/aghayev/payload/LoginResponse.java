package org.aghayev.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Nijat Aghayev on 30.08.19.
 */
@Data
@NoArgsConstructor
public class LoginResponse {
    private Long userId;
    private String accessToken;
    private String refreshToken;

    public LoginResponse(Long userId, String accessToken, String refreshToken) {
        this.userId = userId;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
