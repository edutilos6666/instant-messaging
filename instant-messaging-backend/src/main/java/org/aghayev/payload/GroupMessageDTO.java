package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class GroupMessageDTO {
    private Long userFromId;
    private Long groupChannelId;
    private String content;

    public GroupMessageDTO(Long userFromId, Long groupChannelId, String content) {
        this.userFromId = userFromId;
        this.groupChannelId = groupChannelId;
        this.content = content;
    }
}
