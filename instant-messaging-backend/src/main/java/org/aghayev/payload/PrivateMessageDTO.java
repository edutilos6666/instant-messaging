package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class PrivateMessageDTO {
    private long userFrom;
    private long userTo;
    private long channelId;
    private String content;

    public PrivateMessageDTO(long userFrom, long userTo, long channelId, String content) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.channelId = channelId;
        this.content = content;
    }
}
