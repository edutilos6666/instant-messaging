package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class BroadcastMessageDTO {
    private long userFromId;
    private long channelId;
    private String content;

    public BroadcastMessageDTO(long userFromId, long channelId, String content) {
        this.userFromId = userFromId;
        this.channelId = channelId;
        this.content = content;
    }
}
