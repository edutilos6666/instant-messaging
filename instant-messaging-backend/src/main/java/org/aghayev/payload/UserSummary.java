package org.aghayev.payload;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UserSummary {
    private final Long id;
    private final String username;
    private final String name;
}
