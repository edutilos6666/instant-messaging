package org.aghayev.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@Getter
@Setter
@NoArgsConstructor
public class NotificationDTO {
    private String type;
    private String message;
    private Long fromUserId;

    public NotificationDTO(String type, String message, Long fromUserId) {
        this.type = type;
        this.message = message;
        this.fromUserId = fromUserId;
    }
}
