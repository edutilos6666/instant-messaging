package org.aghayev.payload;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.Instant;


@Data
@RequiredArgsConstructor
public class UserProfile {
    private final Long id;
    private final String name;
    private final String username;
    private final Instant joinedAt;
}
