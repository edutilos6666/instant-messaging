package org.aghayev.repository;

import org.aghayev.model.BroadcastMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */

@Transactional
@Repository
public interface BroadcastMessageRepository extends PagingAndSortingRepository<BroadcastMessage, Long> {
    @Query(
            "FROM BroadcastMessage bm" +
                    " WHERE bm.channel.id = :channelId"
    )
    List<BroadcastMessage> getExistingChatMessagesInChannel(@Param("channelId") long channelId);

    @Query(
            "FROM BroadcastMessage bm" +
                    " WHERE bm.channel.id = :channelId"
    )
    Page<BroadcastMessage> getExistingChatMessagesInChannelWithPage(@Param("channelId") long channelId,
                                                                    Pageable pageable);

    @Query(
            "SELECT COUNT(id) from BroadcastMessage bm" +
                    " WHERE " +
                    " bm.channel.id = :channelId"
    )
    Long findCountByChannelId(@Param("channelId")long channelId);
}
