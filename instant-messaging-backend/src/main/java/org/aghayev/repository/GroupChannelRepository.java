package org.aghayev.repository;

import org.aghayev.model.GroupChannel;
import org.aghayev.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Repository
public interface GroupChannelRepository extends PagingAndSortingRepository<GroupChannel, Long> {
    @Query(
            "FROM GroupChannel gc" +
                    " WHERE" +
                    " gc.owner = :user" +
                    " OR" +
                    " :user MEMBER OF gc.members"
    )
    Page<GroupChannel> findAllByUserId(@Param("user")User user, Pageable pageable);

    @Query(
            "FROM GroupChannel gc" +
            " WHERE" +
            " gc.title = :title"
    )
    List<GroupChannel> findGroupChannelByTitle(@Param("title") String title);

    Page<GroupChannel> findAllByTitle(@Param("title")String title, Pageable pageable);
}
