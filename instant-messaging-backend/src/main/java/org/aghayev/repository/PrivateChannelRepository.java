package org.aghayev.repository;

import org.aghayev.model.PrivateChannel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Repository
public interface PrivateChannelRepository extends PagingAndSortingRepository<PrivateChannel, Long> {
    @Query(
            "from PrivateChannel pc" +
            " WHERE" +
            " pc.userFrom.id IN (:userOneId, :userTwoId)" +
            " AND" +
            " pc.userTo.id IN (:userOneId, :userTwoId)"
    )
    List<PrivateChannel> findPrivateChannelBy(@Param("userOneId") long userOneId,
                                              @Param("userTwoId") long userTwoId);

    @Query(
            "SELECT id" +
            " FROM PrivateChannel pc" +
            " WHERE" +
            " pc.userFrom.id IN (:userOneId, :userTwoId)" +
            " AND" +
            " pc.userTo.id IN (:userOneId, :userTwoId)"
    )
    Long getPrivateChannelIdBy(@Param("userOneId") long userOneId,
                                 @Param("userTwoId") long userTwoId);


    @Query(
            "from PrivateChannel pc" +
            " WHERE " +
            " pc.userFrom.id = :userId" +
            " OR" +
            " pc.userTo.id = :userId"
    )
    Page<PrivateChannel> findPrivateChannelByUserIdWithPage(@Param("userId") long userId,
                                                            Pageable page);


}
