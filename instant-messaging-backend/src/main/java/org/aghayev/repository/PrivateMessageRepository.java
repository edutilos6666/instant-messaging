package org.aghayev.repository;

import org.aghayev.model.PrivateChannel;
import org.aghayev.model.PrivateMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Repository
public interface PrivateMessageRepository extends PagingAndSortingRepository<PrivateMessage, Long> {
    @Query(
            "FROM PrivateMessage pm" +
                    " WHERE" +
                    " pm.userFrom.id IN (:userOneId, :userTwoId)" +
                    " AND" +
                    " pm.userTo.id IN (:userOneId, :userTwoId)"
    )
    List<PrivateMessage> getExistingChatMessagesBetween(@Param("userOneId") long userOneId,
                                                 @Param("userTwoId") long userTwoId);

    @Query(
            "FROM PrivateMessage pm" +
                    " WHERE" +
                    " pm.userFrom.id IN (:userOneId, :userTwoId)" +
                    " AND" +
                    " pm.userTo.id IN (:userOneId, :userTwoId)"
    )
    Page<PrivateMessage> getExistingChatMessagesBetweenPageable(@Param("userOneId") long userOneId,
                                                                @Param("userTwoId") long userTwoId,
                                                                Pageable pageable);
    @Query(
            "SELECT COUNT(id) from PrivateMessage pm" +
                    " WHERE " +
                    " pm.channel.id = :channelId" +
                    " AND" +
                    " (pm.userFrom.id = :userId" +
                    " OR" +
                    " pm.userTo.id = :userId)"
    )
    Long findCountByChannelIdAndUserId(@Param("channelId") long channelId,
                                       @Param("userId") long userId);
}
