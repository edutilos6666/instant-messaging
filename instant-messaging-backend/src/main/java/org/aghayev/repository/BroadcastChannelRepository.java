package org.aghayev.repository;

import org.aghayev.model.BroadcastChannel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */

@Transactional
@Repository
public interface BroadcastChannelRepository extends PagingAndSortingRepository<BroadcastChannel, Long> {
    List<BroadcastChannel> findBroadcastChannelByTitle(@Param("title") String title);
    Page<BroadcastChannel> findAllByTitle(@Param("title")String title, Pageable pageable);
}
