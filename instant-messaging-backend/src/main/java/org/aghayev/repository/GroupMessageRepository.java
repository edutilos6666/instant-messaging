package org.aghayev.repository;

import org.aghayev.model.GroupMessage;
import org.aghayev.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Transactional
@Repository
public interface GroupMessageRepository extends PagingAndSortingRepository<GroupMessage, Long> {
    @Query(
            "FROM GroupMessage gm" +
            " WHERE gm.groupChannel.id = :groupChannelId"
    )
    List<GroupMessage> getExistingChatMessagesInGroupChannel(@Param("groupChannelId") long groupChannelId);
    @Query(
            "FROM GroupMessage gm" +
                    " WHERE gm.groupChannel.id = :groupChannelId"
    )
    Page<GroupMessage> getExistingChatMessagesInGroupChannelWithPage(@Param("groupChannelId") long groupChannelId,
                                                                     Pageable pageable);

    @Query(
            "SELECT COUNT(id) from GroupMessage gm" +
                    " WHERE " +
                    " gm.groupChannel.id = :channelId" +
                    " AND" +
                    " (gm.groupChannel.owner = :user" +
                    " OR" +
                    " :user MEMBER OF gm.groupChannel.members)"
    )
    Long findCountByChannelIdAndUser(@Param("channelId")long channelId,
                                       @Param("user")User user);
}
