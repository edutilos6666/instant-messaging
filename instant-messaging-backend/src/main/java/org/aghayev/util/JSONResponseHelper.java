package org.aghayev.util;

import com.google.gson.GsonBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Service
public class JSONResponseHelper {
    public <T> ResponseEntity<String> createResponse(T responseObj, HttpStatus stat) {
        return new ResponseEntity<>(
                new GsonBuilder()
                        .disableHtmlEscaping()
                        .create()
                        .toJson(responseObj),
                stat
        );
    }
}
