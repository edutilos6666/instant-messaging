package org.aghayev.util;

import org.aghayev.model.BroadcastMessage;
import org.aghayev.model.GroupMessage;
import org.aghayev.model.PrivateMessage;
import org.aghayev.payload.BroadcastMessageDTO;
import org.aghayev.payload.GroupMessageDTO;
import org.aghayev.payload.PrivateMessageDTO;
import org.aghayev.repository.BroadcastChannelRepository;
import org.aghayev.repository.GroupChannelRepository;
import org.aghayev.repository.PrivateChannelRepository;
import org.aghayev.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Service
public class MessageMapper {
    private final UserRepository userRepo;
    private final GroupChannelRepository gcRepo;
    private final PrivateChannelRepository pcRepo;
    private final BroadcastChannelRepository bcRepo;

    @Autowired
    public MessageMapper(
            UserRepository userRepo,
            GroupChannelRepository gcRepo,
            PrivateChannelRepository pcRepo,
            BroadcastChannelRepository bcRepo) {
        this.userRepo = userRepo;
        this.gcRepo = gcRepo;
        this.pcRepo = pcRepo;
        this.bcRepo = bcRepo;
    }

    public PrivateMessage mapPayloadToModel(PrivateMessageDTO payload) {
        return new PrivateMessage(userRepo.findById(payload.getUserFrom()).get(),
                userRepo.findById(payload.getUserFrom()).get(),
                pcRepo.findById(payload.getChannelId()).get(),
                payload.getContent());
    }

    public PrivateMessageDTO mapModelToPayload(PrivateMessage model) {
        return new PrivateMessageDTO(model.getUserFrom().getId(),
                model.getUserTo().getId(),
                model.getChannel().getId(),
                model.getContent());
    }

    public GroupMessage mapPayloadToModel(GroupMessageDTO payload) {
        return new GroupMessage(userRepo.findById(payload.getUserFromId()).get(),
                gcRepo.findById(payload.getGroupChannelId()).get(),
                payload.getContent());
    }

    public GroupMessageDTO mapModelToPayload(GroupMessage model) {
        return new GroupMessageDTO(model.getUserFrom().getId(),
                model.getGroupChannel().getId(),
                model.getContent());
    }

    public BroadcastMessage mapPayloadToModel(BroadcastMessageDTO payload) {
        return new BroadcastMessage(
                userRepo.findById(payload.getUserFromId()).get(),
                bcRepo.findById(payload.getChannelId()).get(),
                payload.getContent()
        );
    }

    public BroadcastMessageDTO mapModelToPayload(BroadcastMessage model) {
        return new BroadcastMessageDTO(
                model.getUserFrom().getId(),
                model.getChannel().getId(),
                model.getContent()
        );
    }
}
