package org.aghayev.util;

import org.aghayev.model.BroadcastChannel;
import org.aghayev.payload.BroadcastChannelDTO;
import org.aghayev.payload.BroadcastMessageDTO;
import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.payload.NotificationDTO;
import org.aghayev.repository.BroadcastChannelRepository;
import org.aghayev.repository.BroadcastMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Service
public class BroadcastChatService {
    private final BroadcastChannelRepository bcRepo;
    private final BroadcastMessageRepository bmRepo;
    private final NotificationService notificationService;
    private final ChannelMapper channelMapper;
    private final MessageMapper messageMapper;

    @Autowired
    public BroadcastChatService(
            BroadcastChannelRepository bcRepo,
            BroadcastMessageRepository bmRepo,
            NotificationService notificationService,
            ChannelMapper channelMapper,
            MessageMapper messageMapper) {
        this.bcRepo = bcRepo;
        this.bmRepo = bmRepo;
        this.notificationService = notificationService;
        this.channelMapper = channelMapper;
        this.messageMapper = messageMapper;
    }


    private Long newChatSession(BroadcastChannelDTO payload) {
      return bcRepo.save(channelMapper.mapPayloadToModel(payload)).getId();
    }

    private Long getExistingChannel(BroadcastChannelDTO payload) {
        List<BroadcastChannel> some=
                bcRepo.findBroadcastChannelByTitle(payload.getTitle());
        return !some.isEmpty()? some.get(0).getId(): null;
    }

    public Long establishChatSesion(BroadcastChannelDTO payload) {
        Long id = getExistingChannel(payload);
        if(id == null) {
            id = newChatSession(payload);
            notificationService.notifyUserAboutBroadcastChannel(
                    new NotificationDTO(NotificationConstants.BROADCAST_CHANNEL_CREATED,
                            NotificationConstants.BROADCAST_CHANNEL_CREATED,
                            payload.getOwnerId())
            );
        }
        return id;
    }




    public void submitMessage(BroadcastMessageDTO payload) {
        bmRepo.save(messageMapper.mapPayloadToModel(payload));
        notificationService.notifyUsersAfterBroadcastSent(
                new MessageCounterDTO(payload.getChannelId(),
                        bmRepo.findCountByChannelId(payload.getChannelId())));
    }

    public List<BroadcastMessageDTO> getExistingChatMessages(long channelId) {
        return bmRepo.getExistingChatMessagesInChannel(channelId)
                .stream()
                .map(one-> messageMapper.mapModelToPayload(one))
                .collect(Collectors.toList());
    }

    public List<MessageCounterDTO> getMessageCountsForChannels() {
        List<MessageCounterDTO> ret = new ArrayList<>();
        bcRepo.findAll()
                .forEach(one->
                        ret.add(new MessageCounterDTO(one.getId(),  bmRepo.findCountByChannelId(one.getId()))));

        return ret;
    }

}
