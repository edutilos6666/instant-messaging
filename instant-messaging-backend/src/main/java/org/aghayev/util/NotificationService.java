package org.aghayev.util;

import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.payload.NotificationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@Service
public class NotificationService {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    public void notifyUserAboutPrivateChannel(long toUserId, NotificationDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.private.channel."+ toUserId, payload);
    }

    public void notifyUsersAfterPrivateMessageSent(long toUserId, MessageCounterDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.private.message."+ toUserId, payload);
    }


    public void notifyUserAboutGroupChannel(long toUserId, NotificationDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.group.channel."+ toUserId, payload);
    }

    public void notifyUsersAfterGroupMessageSent(long toUserId, MessageCounterDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.group.message."+ toUserId, payload);
    }


    public void notifyUserAboutBroadcastChannel(NotificationDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.broadcast.channel", payload);
    }

    public void notifyUsersAfterBroadcastSent(MessageCounterDTO payload) {
        simpMessagingTemplate.convertAndSend("/topic/user.notifcation.broadcast.message", payload);
    }

}
