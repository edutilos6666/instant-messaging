package org.aghayev.util;

import org.aghayev.exception.UserIsSameException;
import org.aghayev.model.PrivateChannel;
import org.aghayev.model.PrivateMessage;
import org.aghayev.model.User;
import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.payload.NotificationDTO;
import org.aghayev.payload.PrivateChatInitializationDTO;
import org.aghayev.payload.PrivateMessageDTO;
import org.aghayev.repository.PrivateChannelRepository;
import org.aghayev.repository.PrivateMessageRepository;
import org.aghayev.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Service
public class PrivateChatService {
    private final PrivateChannelRepository pcRepo;
    private final PrivateMessageRepository pmRepo;
    private final UserRepository userRepo;
    private final MessageMapper pmMapper;
    private final NotificationService notificationService;

    @Autowired
    public PrivateChatService(
            PrivateChannelRepository pcRepo,
            PrivateMessageRepository pmRepo,
            UserRepository userRepo,
            MessageMapper pmMapper,
            NotificationService notificationService) {
        this.pcRepo = pcRepo;
        this.pmRepo = pmRepo;
        this.userRepo = userRepo;
        this.pmMapper = pmMapper;
        this.notificationService = notificationService;
    }

    private Long newChatSession(PrivateChatInitializationDTO payload) {
        User userFrom = userRepo.findById(payload.getUserFromId()).get();
        User userTo = userRepo.findById(payload.getUserToId()).get();

        return pcRepo.save(new PrivateChannel(userFrom, userTo)).getId();
    }

    private Long getExistingChannel(PrivateChatInitializationDTO payload) {
        List<PrivateChannel> some=
                pcRepo.findPrivateChannelBy(payload.getUserFromId(), payload.getUserToId());
        return !some.isEmpty()? some.get(0).getId(): null;
    }

    public Long establishChatSesion(PrivateChatInitializationDTO payload)
     throws UserIsSameException {
        if(payload.getUserFromId() == payload.getUserToId()) {
            throw new UserIsSameException("UserFrom and UserTo are the same");
        }

        Long id = getExistingChannel(payload);
        if(id == null) {
            id = newChatSession(payload);
            notificationService.notifyUserAboutPrivateChannel(payload.getUserToId(),
                    new NotificationDTO(NotificationConstants.PRIVATE_CHANNEL_CREATED,
                            NotificationConstants.PRIVATE_CHANNEL_CREATED,
                            payload.getUserFromId()));
        }
        return id;
    }

    public void submitMessage(long channelId, PrivateMessageDTO payload) {
        User userFrom = userRepo.findById(payload.getUserFrom()).get();
        User userTo = userRepo.findById(payload.getUserTo()).get();
        PrivateChannel channel = pcRepo.findById(payload.getChannelId()).get();
        pmRepo.save(new PrivateMessage(userFrom, userTo, channel, payload.getContent()));
        notificationService.notifyUsersAfterPrivateMessageSent(payload.getUserFrom(),
                new MessageCounterDTO(channelId, pmRepo.findCountByChannelIdAndUserId(channelId, payload.getUserFrom())));
        notificationService.notifyUsersAfterPrivateMessageSent(payload.getUserTo(),
                new MessageCounterDTO(channelId, pmRepo.findCountByChannelIdAndUserId(channelId, payload.getUserTo())));
    }


    public List<PrivateMessageDTO> getExistingChatMessages(long channelId) {
        PrivateChannel privateChannel = pcRepo.findById(channelId).get();
        if(privateChannel == null) return Collections.emptyList();
        return pmRepo.getExistingChatMessagesBetween(privateChannel.getUserFrom().getId(),
                privateChannel.getUserTo().getId())
                .stream()
                .map(one-> pmMapper.mapModelToPayload(one))
                .collect(Collectors.toList());
    }

    public List<MessageCounterDTO> getMessageCountsForChannelsByUserId(String channelIds, long userId) {
        List<MessageCounterDTO> ret = new ArrayList<>();
        Arrays.asList(channelIds.split(","))
                .stream()
                .map(one-> Long.parseLong(one))
                .forEach(channelId->
                        ret.add(new MessageCounterDTO(channelId,  pmRepo.findCountByChannelIdAndUserId(channelId, userId))));

        return ret;
    }
}
