package org.aghayev.util;

import org.aghayev.model.BroadcastChannel;
import org.aghayev.model.GroupChannel;
import org.aghayev.payload.BroadcastChannelDTO;
import org.aghayev.payload.GroupChannelInitializationDTO;
import org.aghayev.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
@Service
public class ChannelMapper {
    private UserRepository userRepo;
    @Autowired
    public ChannelMapper(UserRepository userRepo) {
        this.userRepo = userRepo;
    }
    public GroupChannel mapPayloadToModel(GroupChannelInitializationDTO payload) {
        return new GroupChannel(
          payload.getTitle(),
          userRepo.findById(payload.getOwnerId()).get(),
          payload.getMembersIds().stream().map(one-> userRepo.findById(one).get())
                .collect(Collectors.toList())
        );
    }

    public GroupChannelInitializationDTO mapModelToPayload(GroupChannel model) {
        return new GroupChannelInitializationDTO(
          model.getTitle(),
          model.getOwner().getId(),
          model.getMembers().stream().map(one-> one.getId())
                .collect(Collectors.toList())
        );
    }

    public BroadcastChannel mapPayloadToModel(BroadcastChannelDTO payload) {
        return new BroadcastChannel(
                payload.getTitle(),
                userRepo.findById(payload.getOwnerId()).get()
        );
    }

    public BroadcastChannelDTO mapModelToPayload(BroadcastChannel model) {
        return new BroadcastChannelDTO(
                model.getTitle(),
                model.getOwner().getId()
        );
    }

}
