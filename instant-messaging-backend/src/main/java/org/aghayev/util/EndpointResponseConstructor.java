package org.aghayev.util;

import io.crnk.core.resource.links.DefaultPagedLinksInformation;
import io.crnk.core.resource.list.DefaultResourceList;
import io.crnk.core.resource.list.ResourceList;
import io.crnk.core.resource.meta.DefaultPagedMetaInformation;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Service
public class EndpointResponseConstructor {
    public <T> ResourceList<T> constructResponseForFindAll(Page<T> input) {
        DefaultPagedMetaInformation meta = new DefaultPagedMetaInformation();
        meta.setTotalResourceCount(input.getTotalElements());
        DefaultPagedLinksInformation pageLinks = new DefaultPagedLinksInformation();
        return new DefaultResourceList<T>(input.getContent(), meta, pageLinks);
    }
}
