package org.aghayev.util;

/**
 * Created by Nijat Aghayev on 07.09.19.
 */
public class NotificationConstants {
    public static final String PRIVATE_CHANNEL_CREATED = "PRIVATE_CHANNEL_CREATED";
    public static final String GROUP_CHANNEL_CREATED = "GROUP_CHANNEL_CREATED";
    public static final String BROADCAST_CHANNEL_CREATED = "BROADCAST_CHANNEL_CREATED";
    public static final String MESSAGE_ADDED = "MESSAGE_ADDED";
}
