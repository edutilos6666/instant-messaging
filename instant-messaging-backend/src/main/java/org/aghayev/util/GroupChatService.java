package org.aghayev.util;

import org.aghayev.model.GroupChannel;
import org.aghayev.model.User;
import org.aghayev.payload.GroupChannelInitializationDTO;
import org.aghayev.payload.GroupMessageDTO;
import org.aghayev.payload.MessageCounterDTO;
import org.aghayev.payload.NotificationDTO;
import org.aghayev.repository.GroupChannelRepository;
import org.aghayev.repository.GroupMessageRepository;
import org.aghayev.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 06.09.19.
 */
@Service
public class GroupChatService {
    private final GroupChannelRepository gcRepo;
    private final GroupMessageRepository gmRepo;
    private final UserRepository userRepo;
    private final NotificationService notificationService;
    private final ChannelMapper channelMapper;
    private final MessageMapper messageMapper;

    @Autowired
    public GroupChatService(
            GroupChannelRepository gcRepo,
            GroupMessageRepository gmRepo,
            UserRepository userRepo,
            NotificationService notificationService,
            ChannelMapper channelMapper,
            MessageMapper messageMapper) {
        this.gcRepo = gcRepo;
        this.gmRepo = gmRepo;
        this.userRepo = userRepo;
        this.notificationService = notificationService;
        this.channelMapper = channelMapper;
        this.messageMapper = messageMapper;
    }


    private Long newChatSession(GroupChannelInitializationDTO payload) {
      return gcRepo.save(channelMapper.mapPayloadToModel(payload)).getId();
    }

    private Long getExistingChannel(GroupChannelInitializationDTO payload) {
        List<GroupChannel> some=
                gcRepo.findGroupChannelByTitle(payload.getTitle());
        return !some.isEmpty()? some.get(0).getId(): null;
    }

    public Long establishChatSesion(GroupChannelInitializationDTO payload) {
        Long id = getExistingChannel(payload);
        if(id == null) {
            id = newChatSession(payload);
            payload.getMembersIds().forEach(toUserId-> {
                notificationService.notifyUserAboutGroupChannel(
                        toUserId,
                        new NotificationDTO(NotificationConstants.GROUP_CHANNEL_CREATED,
                                NotificationConstants.GROUP_CHANNEL_CREATED,
                                payload.getOwnerId())
                );
            });
        }
        return id;
    }




    public void submitMessage(GroupMessageDTO payload) {
        gmRepo.save(messageMapper.mapPayloadToModel(payload));
        Optional<GroupChannel> groupChannelOpt = gcRepo.findById(payload.getGroupChannelId());
        groupChannelOpt.ifPresent(groupChannel -> {
            notificationService.notifyUsersAfterGroupMessageSent(groupChannel.getOwner().getId(),
                    new MessageCounterDTO(payload.getGroupChannelId(),
                            gmRepo.findCountByChannelIdAndUser(payload.getGroupChannelId(), groupChannel.getOwner())));

            groupChannel.getMembers().forEach(one -> {
                notificationService.notifyUsersAfterGroupMessageSent(one.getId(),
                        new MessageCounterDTO(payload.getGroupChannelId(),
                                gmRepo.findCountByChannelIdAndUser(payload.getGroupChannelId(), one)));

            });
        });
    }

    public List<GroupMessageDTO> getExistingChatMessages(long channelId) {
        return gmRepo.getExistingChatMessagesInGroupChannel(channelId)
                .stream()
                .map(one-> messageMapper.mapModelToPayload(one))
                .collect(Collectors.toList());
    }

    public List<MessageCounterDTO> getMessageCountsForChannelsByUserId(String channelIds, long userId) {
        User user = userRepo.findById(userId).get();
        List<MessageCounterDTO> ret = new ArrayList<>();
        Arrays.asList(channelIds.split(","))
                .stream()
                .map(one-> Long.parseLong(one))
                .forEach(channelId->
                        ret.add(new MessageCounterDTO(channelId,  gmRepo.findCountByChannelIdAndUser(channelId, user))));

        return ret;
    }

}
