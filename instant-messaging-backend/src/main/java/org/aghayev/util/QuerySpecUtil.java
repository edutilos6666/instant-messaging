package org.aghayev.util;

import io.crnk.core.queryspec.FilterSpec;
import io.crnk.core.queryspec.QuerySpec;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Nijat Aghayev on 29.08.19.
 */
public class QuerySpecUtil {
    public static<T> Optional<T> getFilterValue(QuerySpec querySpec, String filterName) {
        List<FilterSpec> filtered = querySpec.getFilters().stream().filter(f-> f.getAttributePath().get(0).equalsIgnoreCase(filterName))
                .collect(Collectors.toList());
        if(filtered.size() != 1)
            return Optional.empty();
        return Optional.of(filtered.get(0).getValue());
    }
}
