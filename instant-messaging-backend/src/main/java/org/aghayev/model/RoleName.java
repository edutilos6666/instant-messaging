package org.aghayev.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
