package org.aghayev.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiRelation;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */

@Entity
@Table(name="broadcast_message")
@JsonApiResource(type="broadcast-message")
@Getter
@Setter
@NoArgsConstructor
public class BroadcastMessage {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonApiId
    private long id;
    @OneToOne
    @JoinColumn(name = "USER_FROM_ID")
    @JsonApiRelation
    private User userFrom;
    @ManyToOne
    @JoinColumn(name = "CHANNEL_ID")
    @JsonApiRelation
    private BroadcastChannel channel;
    private String content;

    public BroadcastMessage(User userFrom, BroadcastChannel channel, String content) {
        this.userFrom = userFrom;
        this.channel = channel;
        this.content = content;
    }
}
