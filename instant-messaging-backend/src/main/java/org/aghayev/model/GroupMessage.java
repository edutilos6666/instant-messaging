package org.aghayev.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiRelation;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Entity
@Table(name="group_message")
@JsonApiResource(type="group-message")
@Getter
@Setter
@NoArgsConstructor
public class GroupMessage {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonApiId
    private long id;
    @OneToOne
    @JoinColumn(name = "USER_FROM_ID")
    @JsonApiRelation
    private User userFrom;
    @ManyToOne
    @JoinColumn(name = "GROUP_CHANNEL_ID")
    @JsonApiRelation
    private GroupChannel groupChannel;
    private String content;

    public GroupMessage(User userFrom, GroupChannel groupChannel, String content) {
        this.userFrom = userFrom;
        this.groupChannel = groupChannel;
        this.content = content;
    }
}
