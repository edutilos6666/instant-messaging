package org.aghayev.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiRelation;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Entity
@Table(name = "group_channel")
@JsonApiResource(type="group-channel")
@Getter
@Setter
@NoArgsConstructor
public class GroupChannel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonApiId
    private long id;
    private String title;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OWNER_ID")
    @JsonApiRelation
    private User owner;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "group_channel_user",
            joinColumns = { @JoinColumn(name = "GROUP_ID") },
            inverseJoinColumns = { @JoinColumn(name = "USER_ID") }
    )
    @JsonApiRelation
    private List<User> members;

    public GroupChannel(String title, User owner, List<User> members) {
        this.title = title;
        this.owner = owner;
        this.members = members;
    }
}
