package org.aghayev.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiResource;


/**
 * Created by Nijat Aghayev on 27.08.19.
 */

@JsonApiResource(type = "greeting")
public class Greeting {

    @JsonApiId
    private long id;

    private String content;

    public Greeting() {
        super();
    }

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "greeting[id=" + id + ", content=" + content + "]";
    }
}