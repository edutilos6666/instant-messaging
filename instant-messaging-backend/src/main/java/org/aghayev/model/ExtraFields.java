package org.aghayev.model;

import lombok.Data;

/**
 * Created by Nijat Aghayev on 30.08.19.
 */
@Data
public abstract class ExtraFields {
    private boolean onlyCount;
}
