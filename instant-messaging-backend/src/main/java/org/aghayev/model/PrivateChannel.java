package org.aghayev.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiRelation;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Nijat Aghayev on 28.08.19.
 */
@Entity
@Table(name="private_channel")
@JsonApiResource(type="private-channel")
@Getter
@Setter
@NoArgsConstructor
public class PrivateChannel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonApiId
    private long id;
    @OneToOne
    @JoinColumn(name = "USER_FROM_ID")
    @JsonApiRelation
    private User userFrom;
    @OneToOne
    @JoinColumn(name = "USER_TO_ID")
    @JsonApiRelation
    private User userTo;

    public PrivateChannel(User userFrom, User userTo) {
        this.userFrom = userFrom;
        this.userTo = userTo;
    }
}
