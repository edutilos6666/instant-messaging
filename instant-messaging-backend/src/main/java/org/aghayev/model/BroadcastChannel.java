package org.aghayev.model;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiRelation;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Nijat Aghayev on 08.09.19.
 */

@Entity
@Table(name = "broadcast_channel")
@JsonApiResource(type="broadcast-channel")
@Getter
@Setter
@NoArgsConstructor
public class BroadcastChannel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @JsonApiId
    private long id;
    private String title;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OWNER_ID")
    @JsonApiRelation
    private User owner;

    public BroadcastChannel(String title, User owner) {
        this.title = title;
        this.owner = owner;
    }
}
