import { Component } from '@angular/core';
import { User } from './models/User';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './components/login/login.component';
import { AllUsersComponent } from './components/all-users/all-users.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'instant-messaging';
  private user: User = new User(null);
  constructor(private authService: AuthService) {
  }



  onActivate(component) {
    console.group("<<OnActivate>>");
    console.log(component);
    console.groupEnd();

    if(component instanceof AllUsersComponent) {
      this.authService.getCurrentUser().subscribe(user=> this.user = user);
    }
    if(component instanceof LoginComponent) {
      this.user = new User(null);
    }
  }
  onDeactivate(component) {
    console.group("<<OnDeactivate>>");
    console.log(component);
    console.groupEnd();
  }
}
