import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { GroupChannel } from '../models/GroupChannel';

@Injectable({
  providedIn: 'root'
})
export class GroupChannelEndpointService {

  constructor(private datastore: Datastore) { }

  findAll(params: any = []): Observable<JsonApiQueryData<GroupChannel>> {
    return this.datastore.findAll(GroupChannel, params);
  }

  findRecord(channelId: number): Observable<GroupChannel> {
    return this.datastore.findRecord(GroupChannel, String(channelId), {
      include:"owner,members"
    });
  }
}
