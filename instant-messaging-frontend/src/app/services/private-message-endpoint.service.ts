import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { PrivateMessage } from '../models/PrivateMessage';

@Injectable({
  providedIn: 'root'
})
export class PrivateMessageEndpointService {

  constructor(private datastore: Datastore) { }

  findAll(params: any = []): Observable<JsonApiQueryData<PrivateMessage>> {
    return this.datastore.findAll(PrivateMessage, params);
  }

  findRecord(id: number): Observable<PrivateMessage> {
    return this.datastore.findRecord(PrivateMessage, String(id));
  }
}
