import { Injectable } from '@angular/core';
import { HttpHandler, HttpRequest, HttpInterceptor, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class ApiClientInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
    let headers = null;
    if(this.authService.getAccessToken() !== null) {
      headers = req.headers.set("Content-Type", "application/json")
                  .set("Accept", "application/json")
                  .set("Authorization", `Bearer ${this.authService.getAccessToken()}`);
    } else {
      headers = req.headers.set("Content-Type", "application/json")
                   .set("Accept", "application/json");
    }
    const request = req.clone({
      headers: headers
    });

    // request.headers.set("Content-Type", "application/json");
    // request.headers.set("Accept", "application/json");
    // request.headers.set("Authorization", `Bearer ${this.constants.AUTH_TOKEN}`);
    console.log(request.headers);
    return next.handle(request);
  }
}
