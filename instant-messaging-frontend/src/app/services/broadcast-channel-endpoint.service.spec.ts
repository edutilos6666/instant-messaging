import { TestBed } from '@angular/core/testing';

import { BroadcastChannelEndpointService } from './broadcast-channel-endpoint.service';

describe('BroadcastChannelEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BroadcastChannelEndpointService = TestBed.get(BroadcastChannelEndpointService);
    expect(service).toBeTruthy();
  });
});
