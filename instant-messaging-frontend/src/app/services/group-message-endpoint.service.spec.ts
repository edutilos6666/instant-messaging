import { TestBed } from '@angular/core/testing';

import { GroupMessageEndpointService } from './group-message-endpoint.service';

describe('GroupMessageEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupMessageEndpointService = TestBed.get(GroupMessageEndpointService);
    expect(service).toBeTruthy();
  });
});
