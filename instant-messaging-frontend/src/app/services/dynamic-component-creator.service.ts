import { Injectable, Inject, ComponentFactoryResolver, ComponentRef, ComponentFactory } from '@angular/core';
import { SimpleChatEntryComponent } from '../components/simple-chat-entry/simple-chat-entry.component';
import { ChatMessage } from '../models/ChatMessage';

@Injectable()
export class DynamicComponentCreatorService {
  private factoryResolver: ComponentFactoryResolver;
  private rootViewContainer;
  constructor(@Inject(ComponentFactoryResolver) factoryResolver) {
    this.factoryResolver = factoryResolver
  }
  setRootViewContainerRef(viewContainerRef) {
    this.rootViewContainer = viewContainerRef
  }
  addDynamicComponent(chatMessage: ChatMessage, color: string) {
    debugger;
    const factory = this.factoryResolver
                        .resolveComponentFactory(SimpleChatEntryComponent)
    // const component = factory.create(this.rootViewContainer.parentInjector);
    const component = this.rootViewContainer.createComponent(factory);
    component.instance.chatMessage = chatMessage;
    component.instance.color = color;
    debugger;
    // this.rootViewContainer.insert = (component.hostView)
    // debugger;
  }
}
