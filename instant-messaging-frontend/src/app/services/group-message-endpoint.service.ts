import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { GroupMessage } from '../models/GroupMessage';

@Injectable({
  providedIn: 'root'
})
export class GroupMessageEndpointService {

  constructor(private datastore: Datastore) { }

  findAll(params: any = []): Observable<JsonApiQueryData<GroupMessage>> {
    return this.datastore.findAll(GroupMessage, params);
  }

  findRecord(id: number): Observable<GroupMessage> {
    return this.datastore.findRecord(GroupMessage, String(id));
  }
}
