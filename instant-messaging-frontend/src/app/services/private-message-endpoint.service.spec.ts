import { TestBed } from '@angular/core/testing';

import { PrivateMessageEndpointService } from './private-message-endpoint.service';

describe('PrivateMessageEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrivateMessageEndpointService = TestBed.get(PrivateMessageEndpointService);
    expect(service).toBeTruthy();
  });
});
