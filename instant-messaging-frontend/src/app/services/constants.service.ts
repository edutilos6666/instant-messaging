import { Injectable, EventEmitter } from '@angular/core';
import { UserProfile } from '../models/UserProfile';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  constructor() { }
  static BASE_URL: string = "http://localhost:8888";
  static API_URL: string = `${ConstantsService.BASE_URL}/api`;

  SIGNUP_URL: string = `${ConstantsService.API_URL}/auth/signup`;
  SIGNIN_URL: string = `${ConstantsService.API_URL}/auth/signin`;
  REFRESH_TOKEN_URL: string = `${ConstantsService.API_URL}/auth/refresh-token`;
  SOCCER_PLAYERS_URL: string = `${ConstantsService.API_URL}/soccer/players`;
  USERS_URL: string = `${ConstantsService.API_URL}/user`;
  ESTABLISH_PRIVATE_CHANNEL_URL: string = `${ConstantsService.API_URL}/private-chat/channel`;
  // GET_EXISTING_CHAT_MESSAGES_URL: string = `${ConstantsService.API_URL}/private-chat/channel/{channelId}`;
  ESTABLISH_GROUP_CHANNEL_URL: string = `${ConstantsService.API_URL}/group-chat/channel`;
  ESTABLISH_BROADCAST_CHANNEL_URL: string = `${ConstantsService.API_URL}/broadcast-chat/channel`;
  PRIVATE_CHANNEL_MESSAGE_COUNT_URL_BASE: string = `${ConstantsService.API_URL}/private-chat/message-count`;
  GROUP_CHANNEL_MESSAGE_COUNT_URL_BASE: string = `${ConstantsService.API_URL}/group-chat/message-count`;
  BROADCAST_CHANNEL_MESSAGE_COUNT_URL: string = `${ConstantsService.API_URL}/broadcast-chat/message-count`;


  WEBSOCKET_URL: string = `${ConstantsService.BASE_URL}/socket`;
  SIMPLE_CHAT: object = {
    SUBSCRIBE_PATH: "/topic/greetings",
    ADD_USER_PATH: "/app/chat2.addUser",
    SEND_MESSAGE_PATH: "/app/chat2.sendMessage"
  };
  PRIVATE_CHAT: object = {
    SUBSCRIBE_BASE_PATH: "/topic/private.chat.",
    SEND_MESSAGE_BASE_PATH: "/app/private.chat."
  };
  GROUP_CHAT: object = {
    SUBSCRIBE_BASE_PATH: "/topic/group.chat.",
    SEND_MESSAGE_BASE_PATH: "/app/group.chat."
  };

  BROADCAST_CHAT: object = {
    SUBSCRIBE_BASE_PATH: "/topic/broadcast.chat.",
    SEND_MESSAGE_BASE_PATH: "/app/broadcast.chat."
  };

  PRIVATE_CHANNEL_BASE_PATH: string = '/topic/user.notifcation.private.channel.';
  GROUP_CHANNEL_BASE_PATH: string = '/topic/user.notifcation.group.channel.';
  BROADCAST_CHANNEL_PATH: string = '/topic/user.notification.broadcast.channel';
  PRIVATE_MESSAGE_COUNT_BASE_PATH: string = '/topic/user.notifcation.private.message.';
  GROUP_MESSAGE_COUNT_BASE_PATH: string = '/topic/user.notifcation.group.message.';
  BROADCAST_MESSAGE_COUNT_PATH: string = '/topic/user.notifcation.broadcast.message';



  SIGNUP_COMP_PATH: string = "/";
  LOGIN_COMP_PATH: string = "/login";
  USERHOME_COMP_PATH: string = "/userhome";
  CHAT_EXAMPLE_1_COMP_PATH: string = "/chat-example-1";
  CHAT_EXAMPLE_2_COMP_PATH: string  = "/chat-example-2";
  ALL_USERS_COMP_PATH: string = "/all-users";
  USER_DETAILS_COMP_PATH: string = "/user-details";
  GROUP_CHAT_COMP_PATH: string = "/group-chat";
  PRIVATE_CHAT_COMP_PATH: string = "/private-chat";
  BROADCAST_COMP_PATH: string = "/broadcast";

  AUTH_TOKEN: string = null;
  CURRENT_USER: User = null;
  PAGE_SIZE = 13;

  password: string;


  PRIVATE_CHANNEL_EVENT_EMITTER: EventEmitter<number> = new EventEmitter();


}
