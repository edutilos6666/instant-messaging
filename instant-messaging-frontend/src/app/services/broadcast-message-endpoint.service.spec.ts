import { TestBed } from '@angular/core/testing';

import { BroadcastMessageEndpointService } from './broadcast-message-endpoint.service';

describe('BroadcastMessageEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BroadcastMessageEndpointService = TestBed.get(BroadcastMessageEndpointService);
    expect(service).toBeTruthy();
  });
});
