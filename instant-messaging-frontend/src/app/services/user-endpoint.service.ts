import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserEndpointService {

  constructor(private datastore: Datastore) { }

  findByUsername(username: string): Observable<JsonApiQueryData<User>> {
    return this.datastore.findAll(User, {
      filter: {
        username: username
      },
      include: "roles"
    });
  }

  findByOffsetLimit(offset: number , limit: number): Observable<JsonApiQueryData<User>> {
    return this.datastore.findAll(User, {
      page: {
        offset: offset,
        limit: limit
      }
    });
  }

  findRecord(id: string): Observable<User> {
    return this.datastore.findRecord(User, id);
  }

  findCount(): Observable<JsonApiQueryData<User>> {
    return this.datastore.findAll(User, {
      filter: {
        onlyCount: true
      },
      include: "roles"
    });
  }
}
