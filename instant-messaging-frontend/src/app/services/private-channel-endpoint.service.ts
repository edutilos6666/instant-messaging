import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { PrivateChannel } from '../models/PrivateChannel';

@Injectable({
  providedIn: 'root'
})
export class PrivateChannelEndpointService {

  constructor(private datastore: Datastore) { }

  findAll(params: any = []): Observable<JsonApiQueryData<PrivateChannel>> {
    return this.datastore.findAll(PrivateChannel, params);
  }

  findRecord(id: number): Observable<PrivateChannel> {
    return this.datastore.findRecord(PrivateChannel, String(id), {
      include: "userFrom,userTo"
    });
  }
}
