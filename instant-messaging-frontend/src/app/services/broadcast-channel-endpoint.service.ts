import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { BroadcastChannel } from '../models/BroadcastChannel';

@Injectable({
  providedIn: 'root'
})
export class BroadcastChannelEndpointService {

  constructor(private datastore: Datastore) { }

  findAll(params: any = []): Observable<JsonApiQueryData<BroadcastChannel>> {
    return this.datastore.findAll(BroadcastChannel, params);
  }

  findRecord(id: number): Observable<BroadcastChannel> {
    return this.datastore.findRecord(BroadcastChannel, String(id), {
      include: "owner"
    });
  }
}
