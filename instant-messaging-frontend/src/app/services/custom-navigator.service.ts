import { Injectable } from '@angular/core';
import { ConstantsService } from './constants.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CustomNavigatorService {

  constructor(private constants: ConstantsService,
              private router: Router) { }

  showLoginPage() {
    this.router.navigate([this.constants.LOGIN_COMP_PATH]);
  }

  showSignupPage() {
    this.router.navigate([this.constants.SIGNUP_COMP_PATH]);
  }

  showUserhomePage() {
    this.router.navigate([this.constants.USERHOME_COMP_PATH]);
  }

  showChatExample1Page() {
    this.router.navigate([this.constants.CHAT_EXAMPLE_1_COMP_PATH]);
  }

  showChatExample2Page() {
    this.router.navigate([this.constants.CHAT_EXAMPLE_2_COMP_PATH]);
  }

  showAllUsersPage() {
    this.router.navigate([this.constants.ALL_USERS_COMP_PATH]);
  }

  showUserDetailsPage() {
    this.router.navigate([this.constants.USER_DETAILS_COMP_PATH]);
  }

  showGroupChatPage() {
    this.router.navigate([this.constants.GROUP_CHAT_COMP_PATH]);
  }

  showPrivateChatPage() {
    this.router.navigate([this.constants.PRIVATE_CHAT_COMP_PATH]);
  }

  showGroupChatViewPage(channelId: number) {
    this.router.navigate([this.constants.GROUP_CHAT_COMP_PATH, channelId]);
  }

  showPrivateChatViewPage(channelId: number) {
    this.router.navigate([this.constants.PRIVATE_CHAT_COMP_PATH, channelId]);
  }

  showBroadcastPage() {
    this.router.navigate([this.constants.BROADCAST_COMP_PATH]);
  }

  showBroadcastViewPage(channelId: number) {
    this.router.navigate([this.constants.BROADCAST_COMP_PATH, channelId]);
  }

}
