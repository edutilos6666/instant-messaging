import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LoginRequest } from '../models/LoginRequest';
import { LoginResponse } from '../models/LoginResponse';
import { Observable } from 'rxjs';
import { ConstantsService } from './constants.service';
import { UserEndpointService } from './user-endpoint.service';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headers = new HttpHeaders();
  httpOptions = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                              .set('Accept', 'application/json')
  };
  constructor(private httpClient: HttpClient,
              private constants: ConstantsService,
              private userEndpoint: UserEndpointService) { }

  authenticate(loginRequest: LoginRequest): Observable<LoginResponse> {
    console.log(loginRequest);
    return this.httpClient.post<LoginResponse>(this.constants.SIGNIN_URL, loginRequest, this.httpOptions);
  }


  refreshToken(): Observable<LoginResponse> {
    // const body = new URLSearchParams();
    // body.set("grant_type", "refresh_token");
    // body.set("refresh_token", this.getRefreshToken());
    return this.httpClient.post<LoginResponse>(this.constants.REFRESH_TOKEN_URL,
      this.getRefreshToken(), this.httpOptions);
  }


  isLoggedIn() {
    try {
      const userId: number = parseInt(sessionStorage.getItem("USER_ID"));
      return userId !== null && userId !== undefined && !isNaN(userId);
    } catch(ex) {
      return false;
    }
  }

  getCurrentUser(): Observable<User> {
    if(!this.isLoggedIn()) return null;
    return this.userEndpoint.findRecord(sessionStorage.getItem("USER_ID"));
  }


  getAccessToken(): string {
    return sessionStorage.getItem("ACCESS_TOKEN");
  }

  getRefreshToken(): string {
    return sessionStorage.getItem("REFRESH_TOKEN");
  }

  getUserId(): number {
    return parseInt(sessionStorage.getItem("USER_ID"));
  }

  setCredentials(resp: LoginResponse) {
    sessionStorage.setItem("ACCESS_TOKEN", resp.accessToken);
    sessionStorage.setItem("REFRESH_TOKEN", resp.refreshToken);
    sessionStorage.setItem("USER_ID", resp.userId.toString());
  }

  logout() {
    sessionStorage.removeItem("ACCESS_TOKEN");
    sessionStorage.removeItem("REFRESH_TOKEN");
    sessionStorage.removeItem("USER_ID");
    sessionStorage.removeItem("USERNAME");
  }

  setUsername(username: string) {
    sessionStorage.setItem("USERNAME", username);
  }

  getUsername(): string {
    return sessionStorage.getItem("USERNAME");
  }



}
