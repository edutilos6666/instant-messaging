import { Injectable } from '@angular/core';
import { Datastore } from '../jsonapi/Datastore';
import { Observable } from 'rxjs';
import { JsonApiQueryData } from 'angular2-jsonapi';
import { BroadcastMessage } from '../models/BroadcastMessage';

@Injectable({
  providedIn: 'root'
})
export class BroadcastMessageEndpointService {

  constructor(private datastore: Datastore) { }

  findAll(params: any = []): Observable<JsonApiQueryData<BroadcastMessage>> {
    return this.datastore.findAll(BroadcastMessage, params);
  }

  findRecord(id: number): Observable<BroadcastMessage> {
    return this.datastore.findRecord(BroadcastMessage, String(id));
  }
}
