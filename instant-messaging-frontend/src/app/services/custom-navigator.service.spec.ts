import { TestBed } from '@angular/core/testing';

import { CustomNavigatorService } from './custom-navigator.service';

describe('CustomNavigatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomNavigatorService = TestBed.get(CustomNavigatorService);
    expect(service).toBeTruthy();
  });
});
