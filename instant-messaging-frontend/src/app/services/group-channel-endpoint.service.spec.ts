import { TestBed } from '@angular/core/testing';

import { GroupChannelEndpointService } from './group-channel-endpoint.service';

describe('GroupChannelEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupChannelEndpointService = TestBed.get(GroupChannelEndpointService);
    expect(service).toBeTruthy();
  });
});
