import { TestBed } from '@angular/core/testing';

import { DynamicComponentCreatorService } from './dynamic-component-creator.service';

describe('DynamicComponentCreatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicComponentCreatorService = TestBed.get(DynamicComponentCreatorService);
    expect(service).toBeTruthy();
  });
});
