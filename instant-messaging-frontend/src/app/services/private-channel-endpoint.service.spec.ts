import { TestBed } from '@angular/core/testing';

import { PrivateChannelEndpointService } from './private-channel-endpoint.service';

describe('PrivateChannelEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrivateChannelEndpointService = TestBed.get(PrivateChannelEndpointService);
    expect(service).toBeTruthy();
  });
});
