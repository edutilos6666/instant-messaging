import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatExample1Component } from './components/chat-example1/chat-example1.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ChatExample2Component } from './components/chat-example2/chat-example2.component';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { GroupChatComponent } from './components/group-chat/group-chat.component';
import { PrivateChatComponent } from './components/private-chat/private-chat.component';
import { BroadcastComponent } from './components/broadcast/broadcast.component';
import { PrivateChatViewComponent } from './components/private-chat/private-chat-view/private-chat-view.component';
import { UserHomeComponent } from './components/user-home/user-home.component';
import { GroupChatViewComponent } from './components/group-chat/group-chat-view/group-chat-view.component';
import { BroadcastViewComponent } from './components/broadcast/broadcast-view/broadcast-view.component';

const routes: Routes = [
  {path: "", component: SignupComponent},
  {path: "login", component: LoginComponent},
  {path: "userhome", component: UserHomeComponent},
  {path:"chat-example-1", component: ChatExample1Component},
  {path: "chat-example-2", component: ChatExample2Component},
  {path: "all-users", component: AllUsersComponent},
  {path: "user-details", component: UserDetailsComponent},
  {path: "group-chat/:channel-id", component: GroupChatViewComponent},
  {path: "group-chat", component: GroupChatComponent},
  {path: "private-chat/:channel-id", component: PrivateChatViewComponent},
  {path: "private-chat", component: PrivateChatComponent},
  {path: "broadcast/:channel-id", component: BroadcastViewComponent},
  {path: "broadcast", component: BroadcastComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
