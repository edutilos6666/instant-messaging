export class EstablishedGroupChannelDTO {
  channelId: number;
  title: string;
  ownerId: number;
  membersIds: number[];

  constructor(channelId: number = null, title: string = null,
             ownerId: number = null, membersIds: number[] = []) {
    this.channelId = channelId;
    this.title = title;
    this.ownerId = ownerId;
    this.membersIds = [...membersIds];
  }
}
