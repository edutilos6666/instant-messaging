export class NotificationDTO {
  type: string;
  message: string;
  fromUserId: number;

  constructor(type: string = "", message = "", fromUserId: number = null) {
    this.type = type;
    this.message = message;
    this.fromUserId = fromUserId;
  }
}
