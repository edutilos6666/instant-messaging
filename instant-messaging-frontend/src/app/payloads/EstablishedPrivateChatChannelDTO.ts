export class EstablishedPrivateChatChannelDTO {
  channelId: number;
  userFromId: number;
  userToId: number;

  constructor(channelId: number, userFromId: number, userToId: number) {
      this.channelId = channelId;
      this.userFromId = userFromId;
      this.userToId = userToId;
  }
}
