export class PrivateMessageDTO {
  userFrom: number;
  userTo: number;
  channelId: number;
  content: string;

  constructor(userFrom: number = null, userTo: number = null,
              channelId: number = null, content: string = null) {
      this.userFrom = userFrom;
      this.userTo = userTo;
      this.channelId = channelId;
      this.content = content;
  }
}
