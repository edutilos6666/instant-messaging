export class PrivateChatInitializationDTO {
  userFromId: number;
  userToId: number;

  constructor(userFromId: number = 0, userToId: number = 0) {
      this.userFromId = userFromId;
      this.userToId = userToId;
  }
}
