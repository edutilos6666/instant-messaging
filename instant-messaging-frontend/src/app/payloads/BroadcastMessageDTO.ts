export class BroadcastMessageDTO {
  userFromId: number;
  channelId: number;
  content: string;
  constructor(userFromId: number = null, channelId: number = null,
       content: string = null) {
         this.userFromId = userFromId;
         this.channelId = channelId;
         this.content = content;
  }
}
