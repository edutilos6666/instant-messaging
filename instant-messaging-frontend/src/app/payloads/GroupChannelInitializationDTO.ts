export class GroupChannelInitializationDTO {
  title: string;
  ownerId: number;
  membersIds: number[];

  constructor(title: string = null, ownerId: number = null, membersIds: number[] = []) {
    this.title = title;
    this.ownerId = ownerId;
    this.membersIds = [...membersIds];
  }
}
