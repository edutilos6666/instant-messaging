export class BroadcastChannelDTO {
  title: string;
  ownerId: number;

  constructor(title: string = null, ownerId: number = null) {
    this.title = title;
    this.ownerId = ownerId;
  }
}
