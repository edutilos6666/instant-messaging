export class MessageCounterDTO {
  channelId: number;
  messageCount: number;

  constructor(channelId: number = null, messageCount: number = null) {
    this.channelId = channelId;
    this.messageCount = messageCount;
  }
}
