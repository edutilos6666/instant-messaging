export class GroupMessageDTO {
  userFromId: number;
  groupChannelId: number;
  content: string;
  constructor(userFromId: number = null, groupChannelId: number = null,
       content: string = null) {
         this.userFromId = userFromId;
         this.groupChannelId = groupChannelId;
         this.content = content;
  }
}
