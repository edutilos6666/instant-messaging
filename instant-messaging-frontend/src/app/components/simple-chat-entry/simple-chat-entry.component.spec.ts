import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleChatEntryComponent } from './simple-chat-entry.component';

describe('SimpleChatEntryComponent', () => {
  let component: SimpleChatEntryComponent;
  let fixture: ComponentFixture<SimpleChatEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleChatEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleChatEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
