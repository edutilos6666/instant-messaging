import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ChatMessage } from 'src/app/models/ChatMessage';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'app-simple-chat-entry',
  templateUrl: './simple-chat-entry.component.html',
  styleUrls: ['./simple-chat-entry.component.scss']
})
export class SimpleChatEntryComponent implements OnInit {
  @Input() chatMessage: ChatMessage;
  @Input() color: string;

  @ViewChild("avatar", {static: true}) avatar: MatButton;
  constructor() { }

  ngOnInit() {
    this.avatar._elementRef.nativeElement.style.backgroundColor = this.color;
    // this.avatar._elementRef.nativeElement.style.color = this.color;
  }


}
