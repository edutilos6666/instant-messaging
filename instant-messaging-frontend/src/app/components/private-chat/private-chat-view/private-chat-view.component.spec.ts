import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateChatViewComponent } from './private-chat-view.component';

describe('PrivateChatViewComponent', () => {
  let component: PrivateChatViewComponent;
  let fixture: ComponentFixture<PrivateChatViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateChatViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateChatViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
