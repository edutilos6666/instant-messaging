import { Component, OnInit, ViewChild, ElementRef, Inject, ViewContainerRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { ChatMessage } from 'src/app/models/ChatMessage';
import { DynamicComponentCreatorService } from 'src/app/services/dynamic-component-creator.service';
import { SimpleChatEntryComponent } from '../../simple-chat-entry/simple-chat-entry.component';
import { HelperService } from 'src/app/services/helper.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { flatMap } from 'rxjs/operators';
import { PrivateChannelEndpointService } from 'src/app/services/private-channel-endpoint.service';
import { PrivateMessageDTO } from 'src/app/payloads/PrivateMessageDTO';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { PrivateMessageEndpointService } from 'src/app/services/private-message-endpoint.service';
import { PrivateMessage } from 'src/app/models/PrivateMessage';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-private-chat-view',
  templateUrl: './private-chat-view.component.html',
  styleUrls: ['./private-chat-view.component.scss']
})
export class PrivateChatViewComponent implements OnInit, OnDestroy {
  private socketUrl: string;
  private title: string = "Private Chat";
  private stompClient: any;
  usersInChatroom: string;
  chatStatus: string;
  inputText: string;
  color: string = "primary";
  private channelId: number;
  private userFromId: number;
  private userToId: number;
  private userFrom: User;
  private userTo: User;


  service: DynamicComponentCreatorService;

  @ViewChild("chatContainer", {read: ViewContainerRef, static: true}) chatContainer: ViewContainerRef;
  @ViewChild("inputTextEl", {static: true}) inputTextEl: ElementRef;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private helperService: HelperService,
              private factoryResolver: ComponentFactoryResolver,
              private activatedRoute: ActivatedRoute,
              private privateChannelEndpoint: PrivateChannelEndpointService,
              private privateMessageEndpoint: PrivateMessageEndpointService,
              private customNavigator: CustomNavigatorService) {
               }

  ngOnInit() {
    this.activatedRoute.params.pipe(flatMap(params=> {
      const channelId: number = +params["channel-id"];
      this.channelId = channelId;
      return this.privateChannelEndpoint.findRecord(channelId);
    }), flatMap(pc=> {
      console.log(pc.userFrom, pc.userTo, pc.id);
      this.userFromId = +pc.userFrom.id;
      this.userToId = +pc.userTo.id;
      this.userFrom = pc.userFrom;
      this.userTo = pc.userTo;
      if(this.authService.getUserId() !== this.userFromId &&
      this.authService.getUserId() !== this.userToId) {
        this.customNavigator.showAllUsersPage();
        return of("default");
      }

      return this.loadAllMessages();
    })).subscribe(ignored=> {
      console.log(ignored, "here");

      // if(this.authService.getUserId() === +pc.userFrom.id) {
      //   this.userFromId = +pc.userFrom.id;
      //   this.userToId = +pc.userTo.id;
      // } else {
      //   this.userFromId = +pc.userTo.id;
      //   this.userToId = +pc.userFrom.id;
      // }



      this.socketUrl = this.constants.WEBSOCKET_URL;
      if(this.authService.getAccessToken() !== null)
        this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;
      this.initializeWebSocketConnection();
      this.inputTextEl.nativeElement.focus();
    });

  }


  ngOnDestroy() {
    // this.stompClient.send(this.constants.SIMPLE_CHAT["ADD_USER_PATH"] , {}, JSON.stringify({
    //   sender: this.authService.getUsername(),
    //   type: 'LEAVE'
    // }));
  }

  loadAllMessages(): Observable<any> {
    return this.privateMessageEndpoint.findAll({
      filter: {
        userFromId: this.userFromId,
        userToId: this.userToId
      },
      include: "userFrom,userTo"
    }).pipe(flatMap(res=> {
      const messages: PrivateMessage[] = res.getModels();
      console.log(res);
      console.log(messages);
      messages.forEach(m=> {
        this.color = this.helperService.getAvatarColor(m.userFrom.username);
        this.addDynamicComponent2(m, this.color);
      });

      return of("default");
    }));
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.socketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe(that.constants.PRIVATE_CHAT["SUBSCRIBE_BASE_PATH"]+ that.channelId, (payload) => {
        console.log(payload);
        const message: PrivateMessageDTO = JSON.parse(payload.body);
            if(message.content) {
              that.color = that.helperService
                    .getAvatarColor(message.userFrom === +that.userFrom.id?
                      that.userFrom.username: that.userTo.username);
              that.addDynamicComponent(message, that.color);
              // that.chatContainer.nativeElement.append("<div class='message'>"+ "(" + message.type+ ")=> " + message.sender + ": " + message.content+"</div>")
              console.log(message.content);
            }


      });

      // that.stompClient.send(that.constants.SIMPLE_CHAT["ADD_USER_PATH"], {}, JSON.stringify({
      //   sender: that.authService.getUsername(),
      //   type: 'JOIN'
      // }));
    });
  }

  sendMessage(){
    this.stompClient.send(this.constants.PRIVATE_CHAT["SEND_MESSAGE_BASE_PATH"]+ this.channelId , {}, JSON.stringify({
      userFrom: this.authService.getUserId(),
      userTo: this.userToId !== this.authService.getUserId()? this.userToId: this.userFromId,
      channelId: this.channelId,
      content: this.inputText.trim(),
      type: 'CHAT'
    }));
    this.inputText = "";
  }

  handleKeydownInputText(event) {
    if(event.key === "Enter") this.sendMessage();
  }


  addDynamicComponent(chatMessage: PrivateMessageDTO, color: string) {
    const factory = this.factoryResolver
                        .resolveComponentFactory(SimpleChatEntryComponent)
    const component = this.chatContainer.createComponent(factory);
    component.instance.chatMessage = new ChatMessage("", chatMessage.content, chatMessage.userFrom === +this.userFrom.id?
    this.userFrom.username: this.userTo.username);
    component.instance.color = color;
  }

  addDynamicComponent2(chatMessage: PrivateMessage, color: string) {
    const factory = this.factoryResolver
                        .resolveComponentFactory(SimpleChatEntryComponent)
    const component = this.chatContainer.createComponent(factory);
    component.instance.chatMessage = new ChatMessage("", chatMessage.content, chatMessage.userFrom.username);
    component.instance.color = color;
  }

}
