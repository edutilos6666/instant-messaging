import { Component, OnInit } from '@angular/core';
import { UserEndpointService } from 'src/app/services/user-endpoint.service';
import { User } from 'src/app/models/User';
import { flatMap } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ConstantsService } from 'src/app/services/constants.service';
import { PrivateChatInitializationDTO } from 'src/app/payloads/PrivateChatInitializationDTO';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';

@Component({
  selector: 'app-private-chat',
  templateUrl: './private-chat.component.html',
  styleUrls: ['./private-chat.component.scss']
})
export class PrivateChatComponent implements OnInit {
  users: User[];
  selectedUser: User;
  constructor(private userEndpoint: UserEndpointService,
              private authService: AuthService,
              private httpClient: HttpClient,
              private constants: ConstantsService,
              private customNavigator: CustomNavigatorService) { }

  ngOnInit() {
    this.userEndpoint.findCount().pipe(flatMap(res=> {
      return this.userEndpoint.findByOffsetLimit(0, res.getMeta().meta.totalResourceCount);
    })).subscribe(res2=> {
      this.users = res2.getModels().filter(one=> +one.id !== this.authService.getUserId());
    });
  }

  handleBtnWriteMessage(event) {
    this.httpClient.put(this.constants.ESTABLISH_PRIVATE_CHANNEL_URL,
      new PrivateChatInitializationDTO(this.authService.getUserId(), +this.selectedUser.id))
      .subscribe(res=> {
        console.log(res);
        this.customNavigator.showPrivateChatViewPage(+res);
        // this.constants.PRIVATE_CHANNEL_EVENT_EMITTER.emit(this.authService.getUserId());
        // this.constants.PRIVATE_CHANNEL_EVENT_EMITTER.emit(+this.selectedUser.id);
      });
  }

}
