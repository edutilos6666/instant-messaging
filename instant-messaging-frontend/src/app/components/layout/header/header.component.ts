import { Component, Input } from '@angular/core';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { Router } from '@angular/router';
import { ConstantsService } from 'src/app/services/constants.service';
import { User } from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input() currentUser: User;
  constructor(private customNavigator: CustomNavigatorService,
              private authService: AuthService,
              private router: Router,
              private constants: ConstantsService) { }



  logout() {
    this.authService.logout();
    this.customNavigator.showLoginPage();
  }

}
