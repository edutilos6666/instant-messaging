import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserEndpointService } from 'src/app/services/user-endpoint.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { flatMap } from 'rxjs/operators';
import { GroupChannel } from 'src/app/models/GroupChannel';
import { MatSelectionList, MatListOption } from '@angular/material';
import { GroupChannelInitializationDTO } from 'src/app/payloads/GroupChannelInitializationDTO';
import { GroupChannelEndpointService } from 'src/app/services/group-channel-endpoint.service';

@Component({
  selector: 'app-group-chat',
  templateUrl: './group-chat.component.html',
  styleUrls: ['./group-chat.component.scss']
})
export class GroupChatComponent implements OnInit {
  users: User[];
  groupChannelTitle: string = "";
  titleStatus: string = "";
  titleExists: boolean = false;

  @ViewChild("usersSelectionList", {static: true}) usersSelectionList: MatSelectionList;

  constructor(private userEndpoint: UserEndpointService,
              private authService: AuthService,
              private httpClient: HttpClient,
              private constants: ConstantsService,
              private customNavigator: CustomNavigatorService,
              private groupChannelEndpoint: GroupChannelEndpointService) { }

  ngOnInit() {
    this.userEndpoint.findCount().pipe(flatMap(res=> {
      return this.userEndpoint.findByOffsetLimit(0, res.getMeta().meta.totalResourceCount);
    })).subscribe(res2=> {
      this.users = res2.getModels().filter(one=> +one.id !== this.authService.getUserId());
    });
  }


  handleBtnWriteMessage(event) {
    this.authService.getCurrentUser().pipe(flatMap(currentUser=> {
      console.log(currentUser);
      const gc: GroupChannelInitializationDTO = new GroupChannelInitializationDTO(
        this.groupChannelTitle.trim(),
        +currentUser.id,
        this.findSelectedUsers().map(one=> +one.id)
      );
      return  this.httpClient.put(this.constants.ESTABLISH_GROUP_CHANNEL_URL,
        gc);
      })).subscribe(res=> {
          console.log(res);
          this.customNavigator.showGroupChatViewPage(+res);
    });;
  }

  findSelectedUsers(): User[] {
    const selected: MatListOption[] = this.usersSelectionList.selectedOptions.selected;
    const selectedUsers = this.users.filter(user=> {
      return selected.filter(one=> one.getLabel().trim() === user.username).length > 0;
    });
    return selectedUsers;
  }

  disableBtnCreateGroupChannel(): boolean {
    try {
      return this.users.length === 0 || this.usersSelectionList.selectedOptions.selected.length === 0
      || this.groupChannelTitle.trim() === ""
      || this.titleExists;
    } catch(ex) {
      return true;
    }
  }

  handleKeyup(evt) {
    console.log(this.groupChannelTitle);
    this.groupChannelEndpoint.findAll({
      filter: {
        title: this.groupChannelTitle.trim()
      }
    }).subscribe(res=> {
      if(res.getModels().length > 0) {
        this.titleExists = true;
        this.titleStatus = "Title exists.";
      } else {
        this.titleExists = false;
        this.titleStatus = "";
      }
    });
  }


}
