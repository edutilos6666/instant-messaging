import { Component, OnInit, ViewChild, ElementRef, Inject, ViewContainerRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { ChatMessage } from 'src/app/models/ChatMessage';
import { DynamicComponentCreatorService } from 'src/app/services/dynamic-component-creator.service';
import { SimpleChatEntryComponent } from '../../simple-chat-entry/simple-chat-entry.component';
import { HelperService } from 'src/app/services/helper.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { flatMap, switchMap } from 'rxjs/operators';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { Observable, of } from 'rxjs';
import { GroupChannelEndpointService } from 'src/app/services/group-channel-endpoint.service';
import { GroupMessageEndpointService } from 'src/app/services/group-message-endpoint.service';
import { GroupMessage } from 'src/app/models/GroupMessage';
import { GroupChannel } from 'src/app/models/GroupChannel';
import { GroupMessageDTO } from 'src/app/payloads/GroupMessageDTO';
import { UserEndpointService } from 'src/app/services/user-endpoint.service';

@Component({
  selector: 'app-group-chat-view',
  templateUrl: './group-chat-view.component.html',
  styleUrls: ['./group-chat-view.component.scss']
})
export class GroupChatViewComponent implements OnInit {
  private socketUrl: string;
  title: string = "Group Chat";
  private stompClient: any;
  usersInChatroom: string;
  chatStatus: string;
  inputText: string;
  color: string = "primary";
  private channelId: number;
  groupChannel: GroupChannel;
  private userFromId: number;
  // private userToId: number;
  // private userFrom: User;
  // private userTo: User;


  service: DynamicComponentCreatorService;

  @ViewChild("chatContainer", {read: ViewContainerRef, static: true}) chatContainer: ViewContainerRef;
  @ViewChild("inputTextEl", {static: true}) inputTextEl: ElementRef;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private helperService: HelperService,
              private factoryResolver: ComponentFactoryResolver,
              private activatedRoute: ActivatedRoute,
              private groupChannelEndpoint: GroupChannelEndpointService,
              private groupMessageEndpoint: GroupMessageEndpointService,
              private customNavigator: CustomNavigatorService,
              private userEndpoint: UserEndpointService) {
               }

  ngOnInit() {
    this.activatedRoute.params.pipe(
      switchMap(params=> {
      const channelId: number = +params["channel-id"];
      this.channelId = channelId;
      return this.groupChannelEndpoint.findRecord(channelId);
    }), switchMap(gc=> {
      this.groupChannel = gc;
      console.log(gc, gc.title, gc.owner, gc.members);
      if(+gc.owner.id !== this.authService.getUserId() &&
       gc.members.map(one=> +one.id).indexOf(this.authService.getUserId()) <0) {
        this.customNavigator.showAllUsersPage();
        return of("default");
       }

      return this.loadAllMessages();
    })).subscribe(ignored=> {
      console.log(ignored, "here");
      this.socketUrl = this.constants.WEBSOCKET_URL;
      if(this.authService.getAccessToken() !== null)
        this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;
      this.initializeWebSocketConnection();
      this.inputTextEl.nativeElement.focus();
    });

  }


  loadAllMessages(): Observable<any> {
    return this.groupMessageEndpoint.findAll({
      filter: {
        groupChannelId: this.channelId
      },
      include: "userFrom,groupChannel"
    }).pipe(flatMap(res=> {
      const messages: GroupMessage[] = res.getModels();
      console.log(res);
      console.log(messages);
      messages.forEach(m=> {
        this.color = this.helperService.getAvatarColor(m.userFrom.username);
        this.addDynamicComponent2(m, this.color);
      });

      return of("default");
    }));
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.socketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, (frame)=> {
      that.stompClient.subscribe(that.constants.GROUP_CHAT["SUBSCRIBE_BASE_PATH"]+ that.channelId, (payload) => {
        console.log(payload);
        const message: GroupMessageDTO = JSON.parse(payload.body);
            if(message.content) {
              that.addDynamicComponent(message);
              // that.chatContainer.nativeElement.append("<div class='message'>"+ "(" + message.type+ ")=> " + message.sender + ": " + message.content+"</div>")
              console.log(message.content);
            }


      });

    });
  }

  sendMessage(){
    this.authService.getCurrentUser().subscribe(currentUser=> {
      this.stompClient.send(this.constants.GROUP_CHAT["SEND_MESSAGE_BASE_PATH"]+ this.channelId , {}, JSON.stringify({
        userFromId: +currentUser.id,
        groupChannelId: this.channelId,
        content: this.inputText.trim(),
        type: 'CHAT'
      }));
      this.inputText = "";
    });

  }

  handleKeydownInputText(event) {
    if(event.key === "Enter") this.sendMessage();
  }


  addDynamicComponent(chatMessage: GroupMessageDTO) {
    this.userEndpoint.findRecord(chatMessage.userFromId+"").subscribe(userFrom=> {
      const color = this.helperService.getAvatarColor(userFrom.username);
      const factory = this.factoryResolver
          .resolveComponentFactory(SimpleChatEntryComponent)
      const component = this.chatContainer.createComponent(factory);
      component.instance.chatMessage = new ChatMessage("", chatMessage.content, userFrom.username);
      component.instance.color = color;
    });
  }

  addDynamicComponent2(chatMessage: GroupMessage, color: string) {
    const factory = this.factoryResolver
                        .resolveComponentFactory(SimpleChatEntryComponent)
    const component = this.chatContainer.createComponent(factory);
    component.instance.chatMessage = new ChatMessage("", chatMessage.content, chatMessage.userFrom.username);
    component.instance.color = color;
  }

}
