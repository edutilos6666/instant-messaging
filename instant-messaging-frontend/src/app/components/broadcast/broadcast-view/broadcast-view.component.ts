import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { ChatMessage } from 'src/app/models/ChatMessage';
import { DynamicComponentCreatorService } from 'src/app/services/dynamic-component-creator.service';
import { SimpleChatEntryComponent } from '../../simple-chat-entry/simple-chat-entry.component';
import { HelperService } from 'src/app/services/helper.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { flatMap, switchMap } from 'rxjs/operators';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { Observable, of } from 'rxjs';
import { UserEndpointService } from 'src/app/services/user-endpoint.service';
import { BroadcastChannelEndpointService } from 'src/app/services/broadcast-channel-endpoint.service';
import { BroadcastMessageEndpointService } from 'src/app/services/broadcast-message-endpoint.service';
import { BroadcastChannel } from 'src/app/models/BroadcastChannel';
import { BroadcastMessage } from 'src/app/models/BroadcastMessage';
import { BroadcastMessageDTO } from 'src/app/payloads/BroadcastMessageDTO';

@Component({
  selector: 'app-broadcast-view',
  templateUrl: './broadcast-view.component.html',
  styleUrls: ['./broadcast-view.component.scss']
})
export class BroadcastViewComponent implements OnInit {
  private socketUrl: string;
  title: string = "Broadcast Chat";
  private stompClient: any;
  usersInChatroom: string;
  chatStatus: string;
  inputText: string;
  color: string = "primary";
  private channelId: number;
  bcChannel: BroadcastChannel;
  private userFromId: number;
  // private userToId: number;
  // private userFrom: User;
  // private userTo: User;


  service: DynamicComponentCreatorService;

  @ViewChild("chatContainer", {read: ViewContainerRef, static: true}) chatContainer: ViewContainerRef;
  @ViewChild("inputTextEl", {static: true}) inputTextEl: ElementRef;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private helperService: HelperService,
              private factoryResolver: ComponentFactoryResolver,
              private activatedRoute: ActivatedRoute,
              private bcEndpoint: BroadcastChannelEndpointService,
              private bmEndpoint: BroadcastMessageEndpointService,
              private customNavigator: CustomNavigatorService,
              private userEndpoint: UserEndpointService) {
               }

  ngOnInit() {
    this.activatedRoute.params.pipe(
      switchMap(params=> {
      const channelId: number = +params["channel-id"];
      this.channelId = channelId;
      return this.bcEndpoint.findRecord(channelId);
    }), switchMap(bc=> {
      this.bcChannel = bc;
      console.log(bc, bc.title, bc.owner, bc.members);

      return this.loadAllMessages();
    })).subscribe(ignored=> {
      console.log(ignored, "here");
      this.socketUrl = this.constants.WEBSOCKET_URL;
      if(this.authService.getAccessToken() !== null)
        this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;
      this.initializeWebSocketConnection();
      this.inputTextEl.nativeElement.focus();
    });

  }


  loadAllMessages(): Observable<any> {
    return this.bmEndpoint.findAll({
      filter: {
        channelId: this.channelId
      },
      include: "userFrom,channel"
    }).pipe(flatMap(res=> {
      const messages: BroadcastMessage[] = res.getModels();
      console.log(res);
      console.log(messages);
      messages.forEach(m=> {
        this.addDynamicComponent2(m);
      });

      return of("default");
    }));
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.socketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, (frame)=> {
      that.stompClient.subscribe(that.constants.BROADCAST_CHAT["SUBSCRIBE_BASE_PATH"]+ that.channelId, (payload) => {
        console.log(payload);
        const message: BroadcastMessageDTO = JSON.parse(payload.body);
            if(message.content) {
              that.addDynamicComponent(message);
              // that.chatContainer.nativeElement.append("<div class='message'>"+ "(" + message.type+ ")=> " + message.sender + ": " + message.content+"</div>")
              console.log(message.content);
            }


      });

    });
  }

  sendMessage(){
    this.authService.getCurrentUser().subscribe(currentUser=> {
      this.stompClient.send(this.constants.BROADCAST_CHAT["SEND_MESSAGE_BASE_PATH"]+ this.channelId , {}, JSON.stringify({
        userFromId: +currentUser.id,
        channelId: this.channelId,
        content: this.inputText.trim(),
        type: 'CHAT'
      }));
      this.inputText = "";
    });

  }

  handleKeydownInputText(event) {
    if(event.key === "Enter") this.sendMessage();
  }


  addDynamicComponent(chatMessage: BroadcastMessageDTO) {
    this.userEndpoint.findRecord(chatMessage.userFromId+"").subscribe(userFrom=> {
      const color = this.helperService.getAvatarColor(userFrom.username);
      const factory = this.factoryResolver
          .resolveComponentFactory(SimpleChatEntryComponent)
      const component = this.chatContainer.createComponent(factory);
      component.instance.chatMessage = new ChatMessage("", chatMessage.content, userFrom.username);
      component.instance.color = color;
    });
  }

  addDynamicComponent2(chatMessage: BroadcastMessage) {
    const color = this.helperService.getAvatarColor(chatMessage.userFrom.username);
    const factory = this.factoryResolver
                        .resolveComponentFactory(SimpleChatEntryComponent)
    const component = this.chatContainer.createComponent(factory);
    component.instance.chatMessage = new ChatMessage("", chatMessage.content, chatMessage.userFrom.username);
    component.instance.color = color;
  }

}
