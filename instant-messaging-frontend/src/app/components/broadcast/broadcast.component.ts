import { Component, OnInit, ViewChild } from '@angular/core';
import { UserEndpointService } from 'src/app/services/user-endpoint.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { ConstantsService } from 'src/app/services/constants.service';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { flatMap } from 'rxjs/operators';
import { MatSelectionList } from '@angular/material';
import { BroadcastChannelEndpointService } from 'src/app/services/broadcast-channel-endpoint.service';
import { BroadcastChannelDTO } from 'src/app/payloads/BroadcastChannelDTO';

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {
  broadcastChannelTitle: string = "";
  titleStatus: string = "";
  titleExists: boolean = false;

  @ViewChild("usersSelectionList", {static: true}) usersSelectionList: MatSelectionList;

  constructor(private authService: AuthService,
              private httpClient: HttpClient,
              private constants: ConstantsService,
              private customNavigator: CustomNavigatorService,
              private bcEndpoint: BroadcastChannelEndpointService) { }

  ngOnInit() {
  }


  handleBtnWriteMessage(event) {
    this.authService.getCurrentUser().pipe(flatMap(currentUser=> {
      console.log(currentUser);
      const bc: BroadcastChannelDTO = new BroadcastChannelDTO(
        this.broadcastChannelTitle.trim(),
        +currentUser.id
      );
      return  this.httpClient.put(this.constants.ESTABLISH_BROADCAST_CHANNEL_URL,
        bc);
      })).subscribe(res=> {
          console.log(res);
          this.customNavigator.showBroadcastViewPage(+res);
    });;
  }

  disableBtnCreateBroadcastChannel(): boolean {
    try {
      return this.broadcastChannelTitle.trim() === ""
      || this.titleExists;
    } catch(ex) {
      return true;
    }
  }

  handleKeyup(evt) {
    console.log(this.broadcastChannelTitle);
    this.bcEndpoint.findAll({
      filter: {
        title: this.broadcastChannelTitle.trim()
      }
    }).subscribe(res=> {
      if(res.getModels().length > 0) {
        this.titleExists = true;
        this.titleStatus = "Title exists.";
      } else {
        this.titleExists = false;
        this.titleStatus = "";
      }
    });
}
}
