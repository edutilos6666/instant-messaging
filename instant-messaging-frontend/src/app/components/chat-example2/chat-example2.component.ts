import { Component, OnInit, ViewChild, ElementRef, Inject, ViewContainerRef, ComponentFactoryResolver, OnDestroy } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { ChatMessage } from 'src/app/models/ChatMessage';
import { DynamicComponentCreatorService } from 'src/app/services/dynamic-component-creator.service';
import { SimpleChatEntryComponent } from '../simple-chat-entry/simple-chat-entry.component';
import { MatInput } from '@angular/material/input';
import { HelperService } from 'src/app/services/helper.service';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chat-example2',
  templateUrl: './chat-example2.component.html',
  styleUrls: ['./chat-example2.component.scss']
})
export class ChatExample2Component implements OnInit, OnDestroy {
  private socketUrl: string;
  private title: string = "Simple Chat App";
  private stompClient: any;
  usersInChatroom: string;
  chatStatus: string;
  inputText: string;
  color: string = "primary";


  service: DynamicComponentCreatorService;

  @ViewChild("chatContainer", {read: ViewContainerRef, static: true}) chatContainer: ViewContainerRef;
  @ViewChild("inputTextEl", {static: true}) inputTextEl: ElementRef;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private helperService: HelperService,
              private factoryResolver: ComponentFactoryResolver) {
               }

  ngOnInit() {
    this.socketUrl = this.constants.WEBSOCKET_URL;
    if(this.authService.getAccessToken() !== null)
      this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;
    this.initializeWebSocketConnection();
    this.inputTextEl.nativeElement.focus();
    this.setUserCount();
  }


  ngOnDestroy() {
    this.stompClient.send(this.constants.SIMPLE_CHAT["ADD_USER_PATH"] , {}, JSON.stringify({
      sender: this.authService.getUsername(),
      type: 'LEAVE'
    }));
    this.setUserCount(false);
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.socketUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe(that.constants.SIMPLE_CHAT["SUBSCRIBE_PATH"], (payload) => {
        console.log(payload);
        const message: ChatMessage = JSON.parse(payload.body);
        switch(message.type) {
          case "JOIN":
            that.chatStatus = `${message.sender} joined the chat`;
            that.printUsersInChatroom();
            break;
          case "LEAVE":
            that.chatStatus = `${message.sender} left the chat`;
            that.printUsersInChatroom();
            break;
          default:
            if(message.content) {
              that.color = that.helperService.getAvatarColor(message.sender);
              that.addDynamicComponent(message, that.color);
              // that.chatContainer.nativeElement.append("<div class='message'>"+ "(" + message.type+ ")=> " + message.sender + ": " + message.content+"</div>")
              console.log(message.content);
            }
            break;
        }

      });

      that.stompClient.send(that.constants.SIMPLE_CHAT["ADD_USER_PATH"], {}, JSON.stringify({
        sender: that.authService.getUsername(),
        type: 'JOIN'
      }));
    });
  }

  sendMessage(){
    this.stompClient.send(this.constants.SIMPLE_CHAT["SEND_MESSAGE_PATH"] , {}, JSON.stringify({
      sender: this.authService.getUsername(),
      content: this.inputText.trim(),
      type: 'CHAT'
    }));
    this.inputText = "";
  }

  handleKeydownInputText(event) {
    if(event.key === "Enter") this.sendMessage();
  }


  addDynamicComponent(chatMessage: ChatMessage, color: string) {
    const factory = this.factoryResolver
                        .resolveComponentFactory(SimpleChatEntryComponent)
    const component = this.chatContainer.createComponent(factory);
    component.instance.chatMessage = chatMessage;
    component.instance.color = color;
  }


  setUserCount(increment: boolean = true) {
    let userCount: number = parseInt(localStorage.getItem("USER_COUNT")) || 0;
    if(increment) userCount += 1;
    else userCount -= 1;
    localStorage.setItem("USER_COUNT", userCount.toString());
  }
  printUsersInChatroom() {
    const userCount: number = parseInt(localStorage.getItem("USER_COUNT")) || 0;
    this.usersInChatroom = `# of users in chatroom = ${userCount}`;
  }



}
