import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatExample2Component } from './chat-example2.component';

describe('ChatExample2Component', () => {
  let component: ChatExample2Component;
  let fixture: ComponentFixture<ChatExample2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatExample2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatExample2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
