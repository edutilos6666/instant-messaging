import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatExample1Component } from './chat-example1.component';

describe('ChatExample1Component', () => {
  let component: ChatExample1Component;
  let fixture: ComponentFixture<ChatExample1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatExample1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatExample1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
