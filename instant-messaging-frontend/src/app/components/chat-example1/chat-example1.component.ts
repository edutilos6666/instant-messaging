import { Component, OnInit } from '@angular/core';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
import { ConstantsService } from 'src/app/services/constants.service';
import { ChatMessage } from 'src/app/models/ChatMessage';

@Component({
  selector: 'app-chat-example1',
  templateUrl: './chat-example1.component.html',
  styleUrls: ['./chat-example1.component.scss']
})
export class ChatExample1Component implements OnInit {
  // private serverUrl = 'http://192.168.178.23:8888/im/socket';
  private serverUrl: string; // = 'http://localhost:8888/socket'
  private title: string = 'WebSockets chat';
  private stompClient;

  constructor(private constants: ConstantsService){

  }

  ngOnInit() {
    this.serverUrl = this.constants.WEBSOCKET_URL;
    if(this.constants.AUTH_TOKEN !== null)
      this.serverUrl = `${this.serverUrl}?Session=${this.constants.AUTH_TOKEN}`;
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe(that.constants.SIMPLE_CHAT["SUBSCRIBE_PATH"], (payload) => {
        console.log(payload);
        const message: ChatMessage = JSON.parse(payload.body);
        if(message.content) {
          $(".chat").append("<div class='message'>"+ "(" + message.type+ ")=> " + message.sender + ": " + message.content+"</div>")
          console.log(message.content);
        }
      });

      that.stompClient.send(that.constants.SIMPLE_CHAT["ADD_USER_PATH"], {}, JSON.stringify({
        sender: that.constants.CURRENT_USER.username,
        type: 'JOIN'
      }));
    });
  }

  sendMessage(message){
    this.stompClient.send(this.constants.SIMPLE_CHAT["SEND_MESSAGE_PATH"] , {}, JSON.stringify({
      sender: this.constants.CURRENT_USER.username,
      content: message,
      type: 'CHAT'
    }));
    $('#input').val('');
  }
}
