import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import { LoginRequest } from 'src/app/models/LoginRequest';
import { ConstantsService } from 'src/app/services/constants.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginRequest: LoginRequest = new LoginRequest();

  constructor(private authService: AuthService,
              private constants: ConstantsService,
              private customNavigator: CustomNavigatorService,
              private router: Router) {
    this.loginForm = this.createFormGroup();
   }

  createFormGroup() {
    return new FormGroup({
      usernameOrEmail: new FormControl(this.loginRequest.usernameOrEmail,
        [Validators.required]),
      password: new FormControl(this.loginRequest.password,
        [Validators.required])
    });
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form, " and ", this.loginRequest); // loginRequest is empty, why?
    this.authService.authenticate({usernameOrEmail: form["usernameOrEmail"],
    password: form["password"]}).subscribe(resp=> {
      if(resp !== null) {
        this.constants.password = form["password"];
        this.authService.setCredentials(resp);
        this.authService.setUsername(form["usernameOrEmail"]);
        this.authService.getCurrentUser(). subscribe(user=> {
          setInterval(()=> {
            this.authService.refreshToken().subscribe(resp=> {
              if(resp !== null) {
                this.authService.setCredentials(resp);
              }
            });
           }, 604800000);  // 60*1000
          //  this.customNavigator.showAllUsersPage();
            // this.customNavigator.showUserhomePage();
            this.customNavigator.showGroupChatPage();
        });
      }
      // if(tokenHolder !== null && tokenHolder.accessToken !== null) {
      //   this.constants.AUTH_TOKEN = tokenHolder.accessToken;
      //   this.userEndpoint.findByUsername(form["usernameOrEmail"]).subscribe(user=> {
      //     this.constants.CURRENT_USER = user.getModels()[0];
      //     this.router.navigateByUrl("/chat-example-2");
      //   });
      // }
    });
  }

  clearFields() {
    this.loginForm.reset({ loginRequest: new LoginRequest() });
  }


  allFieldsEmpty() {
    const usernameOrEmailValue = this.usernameOrEmail.value;
    const passwordValue = this.password.value;
    return (usernameOrEmailValue === null || usernameOrEmailValue.length === 0) &&
           (passwordValue === null || passwordValue.length === 0);
  }



  // getters
  get usernameOrEmail() {
    return this.loginForm.get("usernameOrEmail");
  }

  get password() {
    return this.loginForm.get("password");
  }
}
