import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupChannelTabComponent } from './group-channel-tab.component';

describe('GroupChannelTabComponent', () => {
  let component: GroupChannelTabComponent;
  let fixture: ComponentFixture<GroupChannelTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupChannelTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupChannelTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
