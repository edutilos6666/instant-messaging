import { Component, OnInit } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { NotificationDTO } from 'src/app/payloads/NotificationDTO';
import { GroupChannelEndpointService } from 'src/app/services/group-channel-endpoint.service';
import { GroupMessageEndpointService } from 'src/app/services/group-message-endpoint.service';
import { GroupChannel } from 'src/app/models/GroupChannel';
import { GroupChannelInitializationDTO } from 'src/app/payloads/GroupChannelInitializationDTO';
import { MessageCounterDTO } from 'src/app/payloads/MessageCounterDTO';

@Component({
  selector: 'app-group-channel-tab',
  templateUrl: './group-channel-tab.component.html',
  styleUrls: ['./group-channel-tab.component.scss']
})
export class GroupChannelTabComponent implements OnInit {
  groupChannels: GroupChannel[];
  messageCounterMap: Map<number, number> = new Map();

  private socketUrl: string;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private gcEndpoint: GroupChannelEndpointService,
              private gmEndpoint: GroupMessageEndpointService,
              private httpClient: HttpClient,
              private customNavigator: CustomNavigatorService) { }

  ngOnInit() {
    this.findAllRelevantGroupChannels(true);
    this.socketUrl = this.constants.WEBSOCKET_URL;
    if(this.authService.getAccessToken() !== null)
      this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;

    // this.constants.PRIVATE_CHANNEL_EVENT_EMITTER.subscribe(userId=> {
    //   console.log("userId = " + userId);
    //   if(userId === this.authService.getUserId()) {
    //     this.findAllRelevantPrivateChannels();
    //   }
    // });
    this.listenToGroupChannelSocket();
    this.listenToGroupMessageCountSocket();
  }

  listenToGroupChannelSocket(){
    let ws = new SockJS(this.socketUrl);
    let stompClient = Stomp.over(ws);
    let that = this;
    stompClient.connect({}, (frame)=> {
      stompClient.subscribe(`${that.constants.GROUP_CHANNEL_BASE_PATH}${that.authService.getUserId()}`, (payload) => {
        const message: NotificationDTO = JSON.parse(payload.body);
        console.log(payload, message);
        this.findAllRelevantGroupChannels();
      });

      // that.stompClient.send(that.constants.SIMPLE_CHAT["ADD_USER_PATH"], {}, JSON.stringify({
      //   sender: that.authService.getUsername(),
      //   type: 'JOIN'
      // }));
    });
  }

  listenToGroupMessageCountSocket() {
    let ws = new SockJS(this.socketUrl);
    let stompClient = Stomp.over(ws);
    let that = this;
    stompClient.connect({}, (frame)=> {
      stompClient.subscribe(`${that.constants.GROUP_MESSAGE_COUNT_BASE_PATH}${that.authService.getUserId()}`, (payload) => {
        const messageCounter: MessageCounterDTO = JSON.parse(payload.body);
        that.messageCounterMap.set(messageCounter.channelId, messageCounter.messageCount);
        console.group("<<MESSAGE_COUNTER_MAP>>");
        console.log(that.messageCounterMap);
        console.log(payload);
        console.log(messageCounter);
        console.groupEnd();
      });
    });
  }


  findAllRelevantGroupChannels(initTime: boolean = false) {
    this.gcEndpoint.findAll({
      filter: {
        userId: this.authService.getUserId()
      },
      include: "owner,members"
    }).subscribe(res=> {
      this.groupChannels = res.getModels();
      if(initTime) {
        this.initMessageCounterMap();
      }
    });
  }

  handleBtnStartGroupChat(event, gc: GroupChannel) {
    this.httpClient.put(this.constants.ESTABLISH_GROUP_CHANNEL_URL,
      new GroupChannelInitializationDTO(gc.title,
        +gc.owner.id,
        gc.members.map(one=> +one.id)))
      .subscribe(res=> {
        console.log(res);
        this.customNavigator.showGroupChatViewPage(+res);
      });
  }



  initMessageCounterMap() {
    const channelIds: string = this.groupChannels.map(one=> one.id).join(",");

    this.httpClient.get(`${this.constants.GROUP_CHANNEL_MESSAGE_COUNT_URL_BASE}/${channelIds}/${this.authService.getUserId()}`)
    .subscribe((payload: MessageCounterDTO[])=> {
      console.log(payload);
      // const messageCounters: MessageCounterDTO[] = payload;
      payload.forEach(one=> {
        this.messageCounterMap.set(one.channelId, one.messageCount);
      });
    });
  }


  getMessageCountByChannelId(channelId: string) {
    return this.messageCounterMap.get(+channelId);
  }


}
