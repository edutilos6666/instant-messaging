import { Component, OnInit } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { NotificationDTO } from 'src/app/payloads/NotificationDTO';
import { GroupChannelEndpointService } from 'src/app/services/group-channel-endpoint.service';
import { GroupMessageEndpointService } from 'src/app/services/group-message-endpoint.service';
import { GroupChannel } from 'src/app/models/GroupChannel';
import { GroupChannelInitializationDTO } from 'src/app/payloads/GroupChannelInitializationDTO';
import { MessageCounterDTO } from 'src/app/payloads/MessageCounterDTO';
import { BroadcastChannelEndpointService } from 'src/app/services/broadcast-channel-endpoint.service';
import { BroadcastMessageEndpointService } from 'src/app/services/broadcast-message-endpoint.service';
import { BroadcastChannel } from 'src/app/models/BroadcastChannel';
import { BroadcastChannelDTO } from 'src/app/payloads/BroadcastChannelDTO';

@Component({
  selector: 'app-broadcast-channel-tab',
  templateUrl: './broadcast-channel-tab.component.html',
  styleUrls: ['./broadcast-channel-tab.component.scss']
})
export class BroadcastChannelTabComponent implements OnInit {
  broadcastChannels: BroadcastChannel[];
  messageCounterMap: Map<number, number> = new Map();

  private socketUrl: string;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private bcEndpoint: BroadcastChannelEndpointService,
              private bmEndpoint: BroadcastMessageEndpointService,
              private httpClient: HttpClient,
              private customNavigator: CustomNavigatorService) { }

  ngOnInit() {
    this.findAllBroadcastChannels(true);
    this.socketUrl = this.constants.WEBSOCKET_URL;
    if(this.authService.getAccessToken() !== null)
      this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;

    // this.constants.PRIVATE_CHANNEL_EVENT_EMITTER.subscribe(userId=> {
    //   console.log("userId = " + userId);
    //   if(userId === this.authService.getUserId()) {
    //     this.findAllRelevantPrivateChannels();
    //   }
    // });
    this.listenToBroadcastChannelSocket();
    this.listenToBroadcastMessageCountSocket();
  }

  listenToBroadcastChannelSocket(){
    let ws = new SockJS(this.socketUrl);
    let stompClient = Stomp.over(ws);
    let that = this;
    stompClient.connect({}, (frame)=> {
      stompClient.subscribe(`${that.constants.BROADCAST_CHANNEL_PATH}`, (payload) => {
        const message: NotificationDTO = JSON.parse(payload.body);
        console.log(payload, message);
        this.findAllBroadcastChannels();
      });

      // that.stompClient.send(that.constants.SIMPLE_CHAT["ADD_USER_PATH"], {}, JSON.stringify({
      //   sender: that.authService.getUsername(),
      //   type: 'JOIN'
      // }));
    });
  }

  listenToBroadcastMessageCountSocket() {
    let ws = new SockJS(this.socketUrl);
    let stompClient = Stomp.over(ws);
    let that = this;
    stompClient.connect({}, (frame)=> {
      stompClient.subscribe(`${that.constants.BROADCAST_MESSAGE_COUNT_PATH}`, (payload) => {
        const messageCounter: MessageCounterDTO = JSON.parse(payload.body);
        that.messageCounterMap.set(messageCounter.channelId, messageCounter.messageCount);
        console.group("<<MESSAGE_COUNTER_MAP>>");
        console.log(that.messageCounterMap);
        console.log(payload);
        console.log(messageCounter);
        console.groupEnd();
      });
    });
  }


  findAllBroadcastChannels(initTime: boolean = false) {
    this.bcEndpoint.findAll({
      include: "owner"
    }).subscribe(res=> {
      this.broadcastChannels = res.getModels();
      if(initTime) {
        this.initMessageCounterMap();
      }
    });
  }

  handleBtnStartBroadcastChat(event, bc: BroadcastChannel) {
    this.httpClient.put(this.constants.ESTABLISH_BROADCAST_CHANNEL_URL,
      new BroadcastChannelDTO(bc.title,
        +bc.owner.id))
      .subscribe(res=> {
        console.log(res);
        this.customNavigator.showBroadcastViewPage(+res);
      });
  }



  initMessageCounterMap() {

    this.httpClient.get(`${this.constants.BROADCAST_CHANNEL_MESSAGE_COUNT_URL}`)
    .subscribe((payload: MessageCounterDTO[])=> {
      console.log(payload);
      // const messageCounters: MessageCounterDTO[] = payload;
      payload.forEach(one=> {
        this.messageCounterMap.set(one.channelId, one.messageCount);
      });
    });
  }


  getMessageCountByChannelId(channelId: string) {
    return this.messageCounterMap.get(+channelId);
  }

}
