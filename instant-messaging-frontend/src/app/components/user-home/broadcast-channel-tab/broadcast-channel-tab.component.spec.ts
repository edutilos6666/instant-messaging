import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BroadcastChannelTabComponent } from './broadcast-channel-tab.component';

describe('BroadcastChannelTabComponent', () => {
  let component: BroadcastChannelTabComponent;
  let fixture: ComponentFixture<BroadcastChannelTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BroadcastChannelTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BroadcastChannelTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
