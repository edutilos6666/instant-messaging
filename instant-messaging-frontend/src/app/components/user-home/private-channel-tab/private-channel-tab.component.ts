import { Component, OnInit } from '@angular/core';
import { ConstantsService } from 'src/app/services/constants.service';
import { AuthService } from 'src/app/services/auth.service';
import { PrivateChannelEndpointService } from 'src/app/services/private-channel-endpoint.service';
import { PrivateChannel } from 'src/app/models/PrivateChannel';
import { PrivateMessageEndpointService } from 'src/app/services/private-message-endpoint.service';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { NotificationDTO } from 'src/app/payloads/NotificationDTO';
import { HttpClient } from '@angular/common/http';
import { PrivateChatInitializationDTO } from 'src/app/payloads/PrivateChatInitializationDTO';
import { CustomNavigatorService } from 'src/app/services/custom-navigator.service';
import { MessageCounterDTO } from 'src/app/payloads/MessageCounterDTO';

@Component({
  selector: 'app-private-channel-tab',
  templateUrl: './private-channel-tab.component.html',
  styleUrls: ['./private-channel-tab.component.scss']
})
export class PrivateChannelTabComponent implements OnInit {
  privateChannels: PrivateChannel[];
  messageCounterMap: Map<number, number> = new Map();
  private socketUrl: string;
  constructor(private constants: ConstantsService,
              private authService: AuthService,
              private pcEndpoint: PrivateChannelEndpointService,
              private pmEndpoint: PrivateMessageEndpointService,
              private httpClient: HttpClient,
              private customNavigator: CustomNavigatorService) { }

  ngOnInit() {
    this.findAllRelevantPrivateChannels(true);
    this.socketUrl = this.constants.WEBSOCKET_URL;
    if(this.authService.getAccessToken() !== null)
      this.socketUrl = `${this.socketUrl}?Session=${this.authService.getAccessToken()}`;

    // this.constants.PRIVATE_CHANNEL_EVENT_EMITTER.subscribe(userId=> {
    //   console.log("userId = " + userId);
    //   if(userId === this.authService.getUserId()) {
    //     this.findAllRelevantPrivateChannels();
    //   }
    // });


    this.listenToPrivateChannelSocket();
    this.listenToPrivateMessageCountSocket();
  }

  listenToPrivateChannelSocket(){
    let ws = new SockJS(this.socketUrl);
    let stompClient = Stomp.over(ws);
    let that = this;
    stompClient.connect({}, (frame)=> {
      stompClient.subscribe(`${that.constants.PRIVATE_CHANNEL_BASE_PATH}${that.authService.getUserId()}`, (payload) => {
        const message: NotificationDTO = JSON.parse(payload.body);
        console.log(payload, message);
        that.findAllRelevantPrivateChannels();
      });

      // that.stompClient.send(that.constants.SIMPLE_CHAT["ADD_USER_PATH"], {}, JSON.stringify({
      //   sender: that.authService.getUsername(),
      //   type: 'JOIN'
      // }));
    });
  }

  listenToPrivateMessageCountSocket() {
    let ws = new SockJS(this.socketUrl);
    let stompClient = Stomp.over(ws);
    let that = this;
    stompClient.connect({}, (frame)=> {
      stompClient.subscribe(`${that.constants.PRIVATE_MESSAGE_COUNT_BASE_PATH}${that.authService.getUserId()}`, (payload) => {
        const messageCounter: MessageCounterDTO = JSON.parse(payload.body);
        that.messageCounterMap.set(messageCounter.channelId, messageCounter.messageCount);
        console.group("<<MESSAGE_COUNTER_MAP>>");
        console.log(that.messageCounterMap);
        console.log(payload);
        console.log(messageCounter);
        console.groupEnd();
      });
    });
  }

  findAllRelevantPrivateChannels(initTime: boolean = false) {
    this.pcEndpoint.findAll({
      filter: {
        userId: this.authService.getUserId()
      },
      include: "userFrom,userTo"
    }).subscribe(res=> {
      this.privateChannels = res.getModels();
      if(initTime) {
        this.initMessageCounterMap();
      }
    });
  }

  handleBtnStartPrivateChat(event, pc: PrivateChannel) {
    const userFormId: number = this.authService.getUserId();
    const userToId: number = +pc.userFrom.id === userFormId? +pc.userTo.id: +pc.userFrom.id;
    this.httpClient.put(this.constants.ESTABLISH_PRIVATE_CHANNEL_URL,
      new PrivateChatInitializationDTO(userFormId, userToId))
      .subscribe(res=> {
        console.log(res);
        this.customNavigator.showPrivateChatViewPage(+res);
      });
  }



  initMessageCounterMap() {
    const channelIds: string = this.privateChannels.map(one=> one.id).join(",");

    this.httpClient.get(`${this.constants.PRIVATE_CHANNEL_MESSAGE_COUNT_URL_BASE}/${channelIds}/${this.authService.getUserId()}`)
    .subscribe((payload: MessageCounterDTO[])=> {
      console.log(payload);
      // const messageCounters: MessageCounterDTO[] = payload;
      payload.forEach(one=> {
        this.messageCounterMap.set(one.channelId, one.messageCount);
      });
    });
  }

  getMessageCountByChannelId(channelId: string) {
    return this.messageCounterMap.get(+channelId);
  }


}
