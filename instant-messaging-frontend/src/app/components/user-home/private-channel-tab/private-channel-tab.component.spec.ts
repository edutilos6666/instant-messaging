import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateChannelTabComponent } from './private-channel-tab.component';

describe('PrivateChannelTabComponent', () => {
  let component: PrivateChannelTabComponent;
  let fixture: ComponentFixture<PrivateChannelTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateChannelTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateChannelTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
