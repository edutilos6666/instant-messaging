import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { User } from 'src/app/models/User';
import { MatButton } from '@angular/material';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-user-cell',
  templateUrl: './user-cell.component.html',
  styleUrls: ['./user-cell.component.scss']
})
export class UserCellComponent implements OnInit {
  @Input() user: User;

  @ViewChild("avatar", {static: true}) avatar: MatButton;
  constructor(private helperService: HelperService) { }

  ngOnInit() {
    this.avatar._elementRef.nativeElement.style.backgroundColor =
        this.helperService.getAvatarColor(this.user.username);
    // this.avatar._elementRef.nativeElement.style.color = this.color;
  }

}
