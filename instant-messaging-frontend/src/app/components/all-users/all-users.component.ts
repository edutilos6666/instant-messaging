import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserEndpointService } from 'src/app/services/user-endpoint.service';
import { AuthService } from 'src/app/services/auth.service';
import  { flatMap } from 'rxjs/operators';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {
  users: User[];
  length;
  pageSize = 15;
  pageSizeOptions: number[] = [3, 6, 15, 45, 75, 105];
  pageEvent: PageEvent;

  constructor(private userEndpoint: UserEndpointService,
              private authService: AuthService) { }

  async ngOnInit() {
    this.authService.getCurrentUser().pipe(flatMap(user=> {
      return this.userEndpoint.findCount()
    })).subscribe(res=> {
      console.log(res.getMeta().meta.totalResourceCount);
      this.length = res.getMeta().meta.totalResourceCount;
      this.populatePage(0, this.pageSize);
    });
  }

  handlePageChangeEvent($event) {
    console.log($event);
    const offset: number = $event.pageIndex * $event.pageSize;
    const limit: number = $event.pageSize;
    this.populatePage(offset, limit);
  }


  populatePage(offset: number , limit: number) {
    this.userEndpoint.findByOffsetLimit(offset, limit).subscribe(res=> {
      this.users = [...res.getModels()];
    });
  }

  getUsers(i): User[] {
    const ret: User[] = [];
    if(this.users[i] !== undefined) ret.push(this.users[i]);
    if(this.users[i+1] !== undefined) ret.push(this.users[i+1]);
    if(this.users[i+2] !== undefined) ret.push(this.users[i+2]);
    return ret;
  }

}
