import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { JsonApiModule } from 'angular2-jsonapi';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input'
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatBadgeModule} from '@angular/material/badge';
import {MatChipsModule} from '@angular/material/chips';

import { AppComponent } from './app.component';
import { ChatExample1Component } from './components/chat-example1/chat-example1.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ApiClientInterceptorService } from './services/api-client-interceptor.service';
import { ChatExample2Component } from './components/chat-example2/chat-example2.component';
import { SimpleChatEntryComponent } from './components/simple-chat-entry/simple-chat-entry.component';
import { DynamicComponentCreatorService } from './services/dynamic-component-creator.service';
import { HeaderComponent } from './components/layout/header/header.component';
import { AllUsersComponent } from './components/all-users/all-users.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { GroupChatComponent } from './components/group-chat/group-chat.component';
import { PrivateChatComponent } from './components/private-chat/private-chat.component';
import { BroadcastComponent } from './components/broadcast/broadcast.component';
import { UserRowComponent } from './components/all-users/user-row/user-row.component';
import { UserCellComponent } from './components/all-users/user-cell/user-cell.component';
import { UserHomeComponent } from './components/user-home/user-home.component';
import { PrivateChatViewComponent } from './components/private-chat/private-chat-view/private-chat-view.component';
import { PrivateChannelTabComponent } from './components/user-home/private-channel-tab/private-channel-tab.component';
import { GroupChannelTabComponent } from './components/user-home/group-channel-tab/group-channel-tab.component';
import { BroadcastChannelTabComponent } from './components/user-home/broadcast-channel-tab/broadcast-channel-tab.component';
import { GroupChatViewComponent } from './components/group-chat/group-chat-view/group-chat-view.component';
import { BroadcastViewComponent } from './components/broadcast/broadcast-view/broadcast-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatExample1Component,
    LoginComponent,
    SignupComponent,
    ChatExample2Component,
    SimpleChatEntryComponent,
    HeaderComponent,
    AllUsersComponent,
    UserDetailsComponent,
    GroupChatComponent,
    PrivateChatComponent,
    BroadcastComponent,
    UserRowComponent,
    UserCellComponent,
    UserHomeComponent,
    PrivateChatViewComponent,
    PrivateChannelTabComponent,
    GroupChannelTabComponent,
    BroadcastChannelTabComponent,
    GroupChatViewComponent,
    BroadcastViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    JsonApiModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatDividerModule,
    MatTabsModule,
    MatListModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatMenuModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatBadgeModule,
    MatChipsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiClientInterceptorService,
      multi: true
    },
    DynamicComponentCreatorService  // i am not using this right now, but i kept it here!!!
  ],
  bootstrap: [AppComponent],
  entryComponents: [SimpleChatEntryComponent]
})
export class AppModule { }
