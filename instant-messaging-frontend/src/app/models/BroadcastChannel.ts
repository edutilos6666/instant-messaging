import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { User } from './User';


@JsonApiModelConfig({
    type: 'broadcast-channel'
})
export class BroadcastChannel extends JsonApiModel {
  @Attribute() title: string;
  @BelongsTo() owner: User;
}
