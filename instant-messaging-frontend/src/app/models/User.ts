import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { Role } from './Role';


@JsonApiModelConfig({
    type: 'user'
})
export class User extends JsonApiModel {
 @Attribute() name: string;
 @Attribute() username: string;
 @Attribute() email: string;
 @Attribute() password: string;
 @HasMany() roles: Role;
}
