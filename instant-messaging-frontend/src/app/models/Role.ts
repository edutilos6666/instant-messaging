import { Attribute, JsonApiModelConfig } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'role'
})
export class Role {
  @Attribute() name: string;
}
