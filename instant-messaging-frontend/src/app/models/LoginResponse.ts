export class LoginResponse {
  userId: number;
  accessToken: string;
  refreshToken: string;
}
