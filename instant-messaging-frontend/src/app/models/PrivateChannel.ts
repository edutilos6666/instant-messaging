import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { User } from './User';


@JsonApiModelConfig({
    type: 'private-channel'
})
export class PrivateChannel extends JsonApiModel {
  @BelongsTo() userFrom: User;
  @BelongsTo() userTo: User;
}
