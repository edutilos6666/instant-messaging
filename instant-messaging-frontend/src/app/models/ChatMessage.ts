export class ChatMessage {
  type: string;
  content: string;
  sender: string;
  constructor(type: string = "", content: string = "", sender: string = "") {
    this.type = type;
    this.content = content;
    this.sender = sender;
  }
}
