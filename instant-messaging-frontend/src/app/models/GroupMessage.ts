import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { User } from './User';
import { GroupChannel } from './GroupChannel';


@JsonApiModelConfig({
    type: 'group-message'
})
export class GroupMessage extends JsonApiModel {
  @BelongsTo() userFrom: User;
  @BelongsTo() groupChannel: GroupChannel;
  @Attribute() content: string;
}
