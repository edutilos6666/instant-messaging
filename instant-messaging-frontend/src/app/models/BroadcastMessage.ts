import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { User } from './User';


@JsonApiModelConfig({
    type: 'broadcast-message'
})
export class BroadcastMessage extends JsonApiModel {
  @BelongsTo() userFrom: User;
  @BelongsTo() channel: BroadcastChannel;
  @Attribute() content: string;
}
