import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { User } from './User';


@JsonApiModelConfig({
    type: 'group-channel'
})
export class GroupChannel extends JsonApiModel {
  @Attribute() title: string;
  @BelongsTo() owner: User;
  @HasMany() members: User[];

}
