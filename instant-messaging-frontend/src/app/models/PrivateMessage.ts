import { JsonApiModelConfig, JsonApiModel, Attribute, HasMany, BelongsTo } from 'angular2-jsonapi';
import { User } from './User';
import { PrivateChannel } from './PrivateChannel';


@JsonApiModelConfig({
    type: 'private-message'
})
export class PrivateMessage extends JsonApiModel {
  @BelongsTo() userFrom: User;
  @BelongsTo() userTo: User;
  @BelongsTo() channel: PrivateChannel;
  @Attribute() content: string;
}
