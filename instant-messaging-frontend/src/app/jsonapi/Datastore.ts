import { JsonApiDatastoreConfig, JsonApiDatastore, DatastoreConfig } from 'angular2-jsonapi';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstantsService } from '../services/constants.service';
import { PrivateChannel } from '../models/PrivateChannel';
import { User } from '../models/User';
import { PrivateMessage } from '../models/PrivateMessage';
import { GroupChannel } from '../models/GroupChannel';
import { GroupMessage } from '../models/GroupMessage';
import { BroadcastChannel } from '../models/BroadcastChannel';
import { BroadcastMessage } from '../models/BroadcastMessage';

const config: DatastoreConfig = {
  baseUrl: ConstantsService.API_URL,
  models: {
    'user': User,
    'private-channel': PrivateChannel,
    'private-message': PrivateMessage,
    'group-channel': GroupChannel,
    'group-message': GroupMessage,
    'broadcast-channel': BroadcastChannel,
    'broadcast-message': BroadcastMessage
  }
}

@Injectable({
  providedIn: 'root'
})
@JsonApiDatastoreConfig(config)
export class Datastore extends JsonApiDatastore {

    constructor(http: HttpClient) {
        super(http);
    }

}
